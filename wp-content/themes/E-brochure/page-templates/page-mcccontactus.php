<?php

    /* Template name: MCC Contact Us */

?>

<html class="mcchtml">

<head>
    <meta name="viewport" content="width=device-width" />
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo (get_stylesheet_directory_uri().'/core/css/old-form-css/send-to-friend2.css'); ?>" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-5333835-9']);
        //_gaq.push(['_trackPageview']);

        (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>

</head>

<body class="mcc">
<?php


    if ( have_posts() ) while ( have_posts() ) : the_post(); // start loop

?>
            <div class="send-to-friend-wrapper">
                 <div class="content-container">
                    <h1><?php echo the_title(); ?></h1>
                    <hr />
                    <?php echo the_content(); ?>
                </div>
            </div> 
           

<?php

    endwhile; // end the loop

    //get_footer();
?>

<script>
    
    if($('.validation_error').length) {
        $('.validation_error').html('There was a problem with your submission. Please fill out all fields.');
    }

    //console.log(history.state);

    $('form').on('submit', function(e) {

        var $_GET = <?php echo json_encode($_GET); ?>;

        var client   = $_GET.client,
            title    = $_GET.title,
            base_url = $_GET.base_url;

        if(undefined === client || undefined === title) {
            //Do not log
            //console.log("one part was blank: " + client + " - " + title);
        }
        else {
            
            var category = client + " - " + title;

            _gaq.push(['_trackEvent', category, "Click", "Global - E-brochure Contact Us"]);
           //console.log("Tracking with: " + client + " - " + title);
        }

    });

</script>


</body>
</html>