<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
?>
  
</div>
<div class="footer">
	<div class="container">

		<div class="col-xs-12 col-md-4 footer-news">
			<?php echo do_shortcode('[gravityform id="23" title="false" ajax="true" description="false"]');?>
		</div>

		<div class="col-xs-12 col-md-5 footer-logo">
		 	<div class="sub-logo">
				<img src="<?php echo get_field('logo','option');?>">
				
			 </div>
			 <div class="border"> </div>
			<div class="contact-info"> 
			 <img src="<?php echo get_field('sub_logo','option');?>">

				<div class="">
					<p><b><a href="tel:+61390907000" style="font-family: 'Raleway-black';"><?php echo get_field('phone','option');?></a></b></p>
					<p> <a href="mailto:<?php echo get_field('email','option');?>"> <?php echo get_field('email','option');?> </a> </p>
				</div>
		</div>
	

		</div>
		<div class="col-xs-12 col-md-3 footer-social">
			<ul class="social">
				<?php $social = get_field('social_media','option');
				foreach($social as $social_icons) { ?>
					<li> 
						<a href="<?php echo $social_icons['link'];?>"><img src="<?php echo $social_icons['image'];?>"></a>
					</li>
				<?php }
				?>
				
			</ul>
		</div>


</div>

<div class="col-xs-12 copyright"> 
		 <p>Copyright © <?php echo date("Y"); ?> eBrochures by <a href="https://www.mmr.com.au">MMR </a>. All rights reserved. Privacy Policy.  
		 
		</p>
		</div>

<a onclick='window.scrollTo({top: 0, behavior: "smooth"});' class="scrolltotop"><img src="<?php bloginfo('stylesheet_directory'); ?>/core/images/scrolltotop.svg"> <br /><br />Back to <br />Top</a>

</div>
 

<?php wp_footer(); ?>
<script>
	//new UISearch( document.getElementById( 'sb-search' ) );
</script>
</body>
</html>