
<div class="row">

<div class=" benefits col-md-12 col-sm-12">
	 <div class="col-md-12 col-lg-5  col-xs-12 leftimage">

         <img src="<?php bloginfo('stylesheet_directory'); ?>/core/images/new-benifit.png">  
    </div>

   <div class="col-md-12 col-sm-12 col-lg-7 col-xs-12 rightsection">
      
      <div class="title "><?php echo get_field('title','option');?> </div>

      <div class="boxes col-sm-12 col-xs-12">

        <?php $box = get_field('benefits','option');
        foreach($box as $boxes) { ?>

             <div class="col-sm-4 col-md-4 ">

                <div class="box-icon"> <img src="<?php echo $boxes['icon'];?>"> </div>
                <div class="box-title"> <?php echo $boxes['title'];?> </div>
                <div class="description"> <?php echo $boxes['description'];?> </div>

             </div>

       <?php }
        ?>

      </div>

  <div class="button col-sm-12 col-xs-12">
      <button class="pink-border get-started">Get started</button>
    </div>


</div>



  
</div>


</div>