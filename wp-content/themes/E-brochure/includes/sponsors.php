<div class="container sponsor">
<h3 class="center"> <?php echo get_field('title_text','option');?></h3>
<p class="center"> <?php echo get_field('sub_title_text','option');?> </p>

<div class="border"> </div>

<div class="sub-title"> <?php echo get_field('sponsors_1_title','option'); ?></div>
<?php $plat_spon = get_field('platinum_sponsors','option');
      
        if($plat_spon)
        {
          echo '<ul class="platinum-sponsors clearfix">';

          foreach($plat_spon as $logos)
          {
            echo '<li><a href="'.$logos['link'].'"><img src="'.$logos['image'].'" /> </a>';
          }

          echo '</ul>';
        } 
        
        ?>
       

        <?php $bronze_spon = get_field('bronze_sponsors','option');
      
        if($bronze_spon)
        { ?>
           <div class="border"> </div>
        <div class="sub-title"><?php echo get_field('sponsors_2_title','option'); ?></div>

          <?php echo '<ul class="platinum-sponsors mob-slider clearfix">';

          foreach($bronze_spon as $logos)
          {
            echo '<li><a href="'.$logos['link'].'"><img src="'.$logos['image'].'" /> </a>';
          }

          echo '</ul>';
          ?>
        <script>
          jQuery(document).ready(function($) {


              $('.mob-slider').slick({
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 4,

                 responsive: [
                  {
                    breakpoint: 769,
                    settings: {
                      slidesToShow: 4,
                      slidesToScroll: 4
                    }
                  },
                  {
                    breakpoint: 760,
                    settings: {
                      slidesToShow: 4,
                      slidesToScroll: 4
                    }
                  }
                ]
                
              });
          });
          </script>
        <?php 
          }
        ?>
       
        <?php $partners = get_field('partners','option');
      
        if($partners)
        { ?>    <div class="border"> </div>
        <div class="sub-title"> <?php echo get_field('sponsors_3_title','option'); ?> </div>

          <?php
          echo '<ul class="platinum-sponsors clearfix">';

          foreach($partners as $logos)
          {
            echo '<li><a href="'.$logos['link'].'"><img src="'.$logos['image'].'" /> </a>';
          }

          echo '</ul>';
        } 
        
        ?>
      
        <?php $partners = get_field('partners_comminuty','option');
      
        if($partners)
        { ?>
             <div class="border"> </div>
          <div class="sub-title"> <?php echo get_field('community_partner','option'); ?> </div>
         <?php  
          echo '<ul class="platinum-sponsors clearfix">';

          foreach($partners as $logos)
          {
            echo '<li><a href="'.$logos['link'].'"><img src="'.$logos['image'].'" /> </a>';
          }

          echo '</ul>';
        } 
        
        ?>
</div>