<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * Pages Template
 */

get_header(); ?>
<?php

$landing_page_object = get_field('landing_page_relationship');

// Custom header as background image
$header_image = get_field('custom_header_image', $landing_page_object->ID);

if (isset($landing_page_object->ID)){
	echo '<style type="text/css">'.get_post_meta($landing_page_object->ID, '_custom_css', true).'</style>';
	$landing_class = " landing-hero";
}

if( !empty($header_image) ){ ?>
<style>
.custom-header-img {
	background-image: url('<?php echo $header_image['sizes'][ 'custom-header' ]; ?>');
}
</style>
<?php } ?>
 


 
<div class="content-body" id="content-wrap" style="background: url('<?php echo get_field('page_background','option'); ?>');  background-position: center;  background-repeat: no-repeat;    background-size: cover;">
	<div class="container">
	<div class="headering-top">
				<div class="col-md-9">
				<h1 style="padding-bottom:0;"><?php the_title(); ?></h1>
				</div>
				<div class="col-md-3">
					<?php // get_template_part( 'loop-header' ); ?>
				</div>
		</div>
		<div id="content-wrap" class="col-1" style="clear:both;" >
			<div class="col-md-12">
				<?php // get_template_part( 'loop-header' ); ?>
			</div>
			
				<div class="col-md-12 col-sm-12 post-content">
				
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php 
						if ( has_post_thumbnail()) {
							the_post_thumbnail();
							 
						 }
						?>

					<?php the_content(); ?>
				<?php endwhile; else : ?>
					<h1>Post Not Found</h1>
				<?php endif; ?>
				</div>
			
					<?php //get_sidebar('external-links-inner'); ?>
		</div><!-- end row -->
	</div><!-- end of .container -->
	<?php //get_template_part( 'includes/sponsors' ); ?>
</div><!-- end of .container -->


<?php get_footer(); ?>