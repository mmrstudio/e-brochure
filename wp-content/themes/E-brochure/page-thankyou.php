<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/* Template name: Thank you */

get_header('thankyou'); ?>
<?php

$landing_page_object = get_field('landing_page_relationship');

// Custom header as background image
$header_image = get_field('custom_header_image', $landing_page_object->ID);

if (isset($landing_page_object->ID)){
	echo '<style type="text/css">'.get_post_meta($landing_page_object->ID, '_custom_css', true).'</style>';
	$landing_class = " landing-hero";
}

if( !empty($header_image) ){ ?>
<style>
.custom-header-img {
	background-image: url('<?php echo $header_image['sizes'][ 'custom-header' ]; ?>');
}
</style>
<?php } ?>
 

 
<div class="content-body" id="content-wrap" style="background: url('<?php echo get_field('page_background','option'); ?>');  background-position: center;  background-repeat: no-repeat;    background-size: cover;">
	<div class="container">
	 
		<div id="content-wrap" class="col-1" style="clear:both;" >
			<div class="col-md-12">
				<?php // get_template_part( 'loop-header' ); ?>
			</div>
			
				<div class="col-md-12 col-sm-12 post-content-thankyou">
				
				<div class="col-md-6">
					<p style="text-align: center;"><img class="size-large wp-image-3481 aligncenter" src="https://www.e-brochures.com.au/wp-content/uploads/2020/03/success.png" alt="" width="550" height="286" /></p>
				</div>
				<div class="col-md-6">
					<div class="success"> Success!</div> 
					<p>We have received your brief and our team will be in touch with you shortly!</p>
					<p >Thanks for choosing to bring your document to life online with eBrochures.</p>
					 
				</div>
			
					<?php //get_sidebar('external-links-inner'); ?>
		</div><!-- end row -->
	</div><!-- end of .container -->
	<?php //get_template_part( 'includes/sponsors' ); ?>
</div><!-- end of .container -->


<?php get_footer(); ?>