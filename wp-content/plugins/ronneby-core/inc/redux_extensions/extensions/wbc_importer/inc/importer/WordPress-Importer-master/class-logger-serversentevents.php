<?php
if(!defined('ABSPATH')) {
	exit;
}

class WP_Importer_Logger_ServerSentEvents extends WP_Importer_Logger {
	public $text = '';
	
	public function __construct() {
		add_action('import_end', array($this, 'printLog'));
	}
	/**
	 * Print log to file.
	 *
	 * @return null
	 */
	public function printLog() {
		if(class_exists('ReduxFramework')) {
			$uploadsDir = ReduxFramework::$_upload_dir;
			
			if(empty($uploadsDir)) {
				return false;
			}

			global $wp_filesystem;
			if (empty($wp_filesystem)) {
				require_once (ABSPATH . '/wp-admin/includes/file.php');
				WP_Filesystem();
			}
			if( !empty($wp_filesystem) && !(is_wp_error($wp_filesystem->errors) && $wp_filesystem->errors->get_error_code()) ) {
				$wp_filesystem->put_contents(
					$uploadsDir . 'import-log.txt',
					$this->text,
					FS_CHMOD_FILE
				);
			} else {
				file_put_contents($uploadsDir . 'import-log.txt', $this->text);
			}
		}
	}
	/**
	 * Logs with an arbitrary level.
	 *
	 * @param mixed $level
	 * @param string $message
	 * @param array $context
	 * @return null
	 */
	public function log( $level, $message, array $context = array() ) {
		$data = compact( 'level', 'message' );

		switch ( $level ) {
			case 'emergency':
			case 'alert':
			case 'critical':
				echo "event: log\n";
				echo 'data: ' . wp_json_encode( $data ) . "\n\n";
				flush();
				break;
			case 'error':
			case 'warning':
			case 'notice':
			case 'info':
				ob_start();
				echo "event: log\n";
				echo 'data: ' . wp_json_encode( $data ) . "\n\n";
				$output = ob_get_flush();
				$this->text .= $output;
				break;

			case 'debug':
				if ( defined( 'IMPORT_DEBUG' ) && IMPORT_DEBUG ) {
					echo "event: log\n";
					echo 'data: ' . wp_json_encode( $data ) . "\n\n";
					flush();
					break;
				}
				break;
		}
	}
}
