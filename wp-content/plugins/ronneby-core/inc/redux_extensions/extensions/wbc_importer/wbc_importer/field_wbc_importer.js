/* global redux_change, wp */
(function($) {
    "use strict";
    $.redux = $.redux || {};
    
    $.redux.wbc_importer = function() {
		
		var $parent,
			reimport = false,
			allowInstall = false,
			popupHtml = '<div id="dfd-import-popup">\n\
							<div class="dfd-import-popup-content">\n\
								<div class="dfd-import-popup-image"></div>\n\
								<h3></h3>\n\
								<div class="dfd-importer-plugins">\n\
									<ul></ul>\n\
								</div>\n\
								<div class="dfd-importer-checkbox">\n\
									<input type="checkbox" value="enable" /><span>I know that my current settings will be overwritten.</span>\n\
								</div>\n\
								<div class="dfd-importer-buttons">\n\
									<a href="#" title="Run importer" class="dfd-run-importer">Import demo</a>\n\
									<a href="" title="Run importer" target="_blank" class="dfd-see-demo"></a>\n\
								</div>\n\
							</div>\n\
							<a href="#" title="" class="dfd-hide-importer-popup"></a>\n\
						</div>',
			processText = {
				loading: 'Please wait, demo installation is running',
				widgets: 'Installing Widgtes',
				options: 'Installing Options',
				content: 'Installing Content',
				done: 'Done'
			},
			data = {},
			popupContent = {
				thumb: '',
				title: 'Demo',
				plugins: '',
				demoLink: ''
			},
			init = function() {
				getLoadingTexts();
				initClick();
				hidePopUpTrigger();
				initCheckbox();
			},
			getLoadingTexts = function() {
				var $container = $('.wbc_importer');
				
				if($container.data('loading-text')) {
					processText.loading = $container.attr('data-loading-text');
				}

				if($container.data('widgets')) {
					processText.widgets = $container.attr('data-widgets');
				}

				if($container.data('options')) {
					processText.options = $container.attr('data-options');
				}

				if($container.data('content')) {
					processText.content = $container.attr('data-content');
				}

				if($container.data('done')) {
					processText.done = $container.attr('data-done');
				}
			},
			getPopupContent = function() {
				if($parent.find('.image-inner').length > 0) {
					popupContent.thumb = $parent.find('.image-inner').css('backgroundImage');
				}

				if($parent.find('h3.theme-name').length > 0) {
					popupContent.title = $parent.find('h3.theme-name').text();
				}

				var plugins,
					pluginsHtml = '';

				if($parent.find('.theme-actions').length > 0 && $parent.find('.theme-actions').data('plugins')) {
					plugins = JSON.parse(decodeURIComponent($parent.find('.theme-actions').data('plugins')));

					var count = 0;

					$.each(plugins, function(i, val) {
						if(typeof val.title != 'undefined' && typeof val.status != 'undefined') {
							var className = '';
							if(val.status.stat != '1') {
								count++;
								className = 'inactive';
							}
							pluginsHtml += '<li>'+val.title.replace(/\+/g, ' ')+'<span class="'+ className +'">'+ val.status.text.replace(/\+/g, ' ') +'</span></li>';
						} else if(typeof val.text != 'undefined' && typeof val.link != 'undefined' && count != 0) {
							pluginsHtml += '<li class="install-plugins"><a href="'+val.link+'" target="_blank">'+ val.text.replace(/\+/g, ' ') +'</a></li>';
						}
					});
				}

				popupContent.plugins = pluginsHtml;

				if($parent.find('.dfd-import-see-demo').length > 0) {
					popupContent.demoLink = $parent.find('.dfd-import-see-demo').attr('href');
				}
			},
			initClick = function() {
				$('.wrap-importer.theme.not-imported:not(.processing), .wbc-importer-reimport:not(.processing)').on('click', function(e) {
					if(!$(e.target).hasClass('dfd-import-see-demo')) {
						e.preventDefault();
						
						$parent = $(this);
						
						$parent.addClass('processing');

						reimport = false;

						if($(e.target).hasClass('wbc-importer-reimport')) {
							reimport = true;

							if(!$(this).hasClass('rendered')) {
								$parent = $(this).parents('.wrap-importer');
							}
						}

						$parent.addClass('import-started');

						if($parent.hasClass('imported') && reimport == false) return;

						if(reimport == true) {
							$parent.addClass('not-imported');
						}

						$parent.removeClass('active imported');
						
						data = $(this).data();
						data.action = "redux_wbc_importer";
						data.demo_import_id = $parent.attr("data-demo-id");
						data.nonce = $parent.attr("data-nonce");
						data.type = 'import-demo-content';
						data.wbc_import = (reimport == true) ? 're-importing' : ' ';
						
						getPopupContent();
						
						showPopUp();
						
						initImport();
						
						$parent.removeClass('processing');

						return false;
					}
				});
			},
			showPopUp = function() {
				$('body').append(popupHtml);
				
				$('#dfd-import-popup .dfd-import-popup-content').find('h3').text(popupContent.title);
				
				$('#dfd-import-popup .dfd-import-popup-content').find('ul').html(popupContent.plugins);
				
				$('#dfd-import-popup .dfd-import-popup-content').find('.dfd-see-demo').attr('href', popupContent.demoLink);
				
				if(popupContent.thumb !== '') {
					$('#dfd-import-popup .dfd-import-popup-content .dfd-import-popup-image').css('backgroundImage', popupContent.thumb);
				}
				
				if(popupContent.demoLink !== '') {
					$('#dfd-import-popup .dfd-import-popup-content .dfd-importer-buttons .dfd-see-demo').attr('href', popupContent.demoLink);
				}
				
				setTimeout(function() {
					$('#dfd-import-popup').addClass('active');
				}, 300);
			},
			hidePopUp = function() {
				$('#dfd-import-popup').removeClass('active');
				
				setTimeout(function() {
					$('#dfd-import-popup').remove();
				}, 300);
			},
			hidePopUpTrigger = function() {
				$('body').on('click', '.dfd-hide-importer-popup', function(e) {
					e.preventDefault();
					hidePopUp();
				});
			},
			initCheckbox = function() {
				$('body').on('click', '.dfd-importer-checkbox input[type="checkbox"]', function(e) {
					if($(this).is(':checked')) {
						allowInstall = true;
						$('#dfd-import-popup .dfd-import-popup-content .dfd-importer-buttons .dfd-run-importer').addClass('active');
					} else {
						allowInstall = false;
						$('#dfd-import-popup .dfd-import-popup-content .dfd-importer-buttons .dfd-run-importer').removeClass('active');
					}
				});
			},
			buildImportPopupHtml = function() {
				$('#dfd-import-popup .dfd-hide-importer-popup').css('pointer-events', 'none');
				
				var importPopupHtml =  '<div class="inner import-started">\n\
											<h3>'+
												popupContent.title+'\
												<span>'+processText.loading+'</span>\n\
											</h3>\n\
											<div class="content">\n\
												<div class="description widgets-bar">'+
													processText.widgets
													+'<span class="done-label">'+processText.done+'</span>\n\
													<p class="progress"><span></span></p>\n\
												</div>\n\
												<div class="description options-bar">'+
													processText.options
													+'<span class="done-label">'+processText.done+'</span>\n\
													<p class="progress"><span></span></p>\n\
												</div>\n\
												<div class="description content-bar">'+
													processText.content
													+'<span class="done-label">'+processText.done+'</span>\n\
													<p class="progress"><span></span></p>\n\
												</div>\n\
											</div>\n\
										</div>';
				
				$('#dfd-import-popup .dfd-import-popup-content').html(importPopupHtml);
			},
			initImport = function() {
				$('body')
					.off('click', '#dfd-import-popup .dfd-import-popup-content .dfd-importer-buttons .dfd-run-importer:not(.processing)')
					.on('click', '#dfd-import-popup .dfd-import-popup-content .dfd-importer-buttons .dfd-run-importer:not(.processing)', function(e) {
						e.preventDefault();

						if(!allowInstall) {
							return false;
						}

						buildImportPopupHtml();
						
						$(this).addClass('processing');

						var failed = 0,
							currentData = data;

						currentData.action = 'redux_wbc_importer_widgets';

						$.ajax({
							type: 'POST',
							url: ajaxurl,
							data: currentData,
							dataType: 'html',
							beforeSend: function(xhr) {
								$('#dfd-import-popup .widgets-bar').find('.progress > span').css({
									'width': '0%',
									'-webkit-transition': 'width 2s ease',
									'-moz-transition': 'width 2s ease',
									'-o-transition': 'width 2s ease',
									'transition': 'width 2s ease'
								});
								setTimeout(function() {
									$('#dfd-import-popup .widgets-bar').find('.progress > span').css({
										'width': '90%',
										'-webkit-transition': 'width 2s ease',
										'-moz-transition': 'width 2s ease',
										'-o-transition': 'width 2s ease',
										'transition': 'width 2s ease'
									});
								}, 0);
							},
							complete: function(XMLHttpRequest) {
								if(XMLHttpRequest.status == 200 && XMLHttpRequest.responseText == '1') {
									$('#dfd-import-popup .widgets-bar').find('.progress > span').css({
										'width': '100%',
										'-webkit-transition': 'width .1s ease',
										'-moz-transition': 'width .1s ease',
										'-o-transition': 'width .1s ease',
										'transition': 'width .1s ease'
									});
									$('#dfd-import-popup .widgets-bar').find('.done-label').show();
								} else {
									$('#dfd-import-popup .widgets-bar').find('.done-label').text('Failed').css('color', '#9ba1ad').show();
									failed++;
									alert('There was an error importing demo content: \n\n' + XMLHttpRequest.responseText.replace(/(<([^>]+)>)/gi, ""));
								}
								currentData.action = 'redux_wbc_importer_options';

								$.ajax({
									type: 'POST',
									url: ajaxurl,
									data: currentData,
									dataType: 'html',
									beforeSend: function(xhr) {
										$('#dfd-import-popup .options-bar').find('.progress > span').css({
											'width': '90%',
											'-webkit-transition': 'width 2s ease',
											'-moz-transition': 'width 2s ease',
											'-o-transition': 'width 2s ease',
											'transition': 'width 2s ease'
										});
									},
									complete: function(XMLHttpRequest) {
										if(XMLHttpRequest.status == 200 && XMLHttpRequest.responseText == '1') {
											$('#dfd-import-popup .options-bar').find('.progress > span').css({
												'width': '100%',
												'-webkit-transition': 'width .1s ease',
												'-moz-transition': 'width .1s ease',
												'-o-transition': 'width .1s ease',
												'transition': 'width .1s ease'
											});
											$('#dfd-import-popup .options-bar').find('.done-label').show();
										} else {
											$('#dfd-import-popup .options-bar').find('.done-label').text('Failed').css('color', '#9ba1ad').show();
											failed++;
											alert('There was an error importing demo content: \n\n' + XMLHttpRequest.responseText.replace(/(<([^>]+)>)/gi, ""));
										}
										currentData.action = 'redux_wbc_importer_content';

										$.ajax({
											type: 'POST',
											url: ajaxurl,
											data: currentData,
											dataType: 'html',
											beforeSend: function(xhr) {
												$('#dfd-import-popup .content-bar').find('.progress > span').css({
													'width': '80%',
													'-webkit-transition': 'width 600s ease',
													'-moz-transition': 'width 600s ease',
													'-o-transition': 'width 600s ease',
													'transition': 'width 600s ease'
												});
											},
											complete: function(XMLHttpRequest) {
												$('#dfd-import-popup .dfd-hide-importer-popup').css('pointer-events', '');

												if (XMLHttpRequest.status == 200 && XMLHttpRequest.responseText != '' && failed == 0) {
													if (reimport == false) {
														$parent.addClass('rendered').find('.wbc-importer-buttons .importer-button').removeClass('import-demo-data');
														$parent.find('.theme-screenshot').append('<div class="wbc-importer-buttons importer-button dfd-imported-badge"></div>');
													}
													$parent.addClass('imported active').removeClass('not-imported import-started');
													$('#dfd-import-popup .content-bar').find('.progress > span').css({
														'width': '100%',
														'-webkit-transition': 'width 1s ease',
														'-moz-transition': 'width 1s ease',
														'-o-transition': 'width 1s ease',
														'transition': 'width 1s ease'
													});
													$('#dfd-import-popup .content-bar').find('.done-label').show();
													setTimeout(function() {
														hidePopUp();
														var href = location.href + '&success=1&demo=' + popupContent.title;
														location.href = href;
													}, 2000);
												} else {
													$parent.find('.import-demo-data').show();
													if (reimport == true) {
														$parent.addClass('imported active').removeClass('not-imported import-started');
													}
													$('#dfd-import-popup .content-bar').find('.done-label').text('Failed').css('color', '#9ba1ad').show();
													alert('There was an error importing demo content: \n\n' + XMLHttpRequest.responseText.replace(/(<([^>]+)>)/gi, ""));

													hidePopUp();
												}
											}
										});
									}
								});
							}
						});

					$(this).removeClass('processing');
				});
			};
		
		return init();
    };
	
	$(document).ready(function() {
        $.redux.wbc_importer();
    });
})(jQuery);