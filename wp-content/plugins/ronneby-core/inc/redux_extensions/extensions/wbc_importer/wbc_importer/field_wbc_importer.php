<?php
/**
 * Extension-Boilerplate
 * @link https://github.com/ReduxFramework/extension-boilerplate
 *
 * Radium Importer - Modified For ReduxFramework
 * @link https://github.com/FrankM1/radium-one-click-demo-install
 *
 * @package     WBC_Importer - Extension for Importing demo content
 * @author      Webcreations907
 * @version     1.0.1
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// Don't duplicate me!
if ( !class_exists( 'ReduxFramework_wbc_importer' ) ) {

    /**
     * Main ReduxFramework_wbc_importer class
     *
     * @since       1.0.0
     */
    class ReduxFramework_wbc_importer {

        /**
         * Field Constructor.
         *
         * @since       1.0.0
         * @access      public
         * @return      void
         */
        function __construct( $field = array(), $value ='', $parent ) {
            $this->parent = $parent;
            $this->field = $field;
            $this->value = $value;

            $class = ReduxFramework_extension_wbc_importer::get_instance();

            if ( !empty( $class->demo_data_dir ) ) {
                $this->demo_data_dir = $class->demo_data_dir;
				$this->demo_data_url = get_template_directory_uri().'/inc/demo-data/';
//				$this->demo_data_url = site_url( str_replace( trailingslashit( str_replace( '\\', '/', ABSPATH ) ), '', $this->demo_data_dir ) );
            }

            if ( empty( $this->extension_dir ) ) {
                $this->extension_dir = trailingslashit( str_replace( '\\', '/', dirname( __FILE__ ) ) );
                $this->extension_url = site_url( str_replace( trailingslashit( str_replace( '\\', '/', ABSPATH ) ), '', $this->extension_dir ) );
            }
        }

        /**
         * Field Render Function.
         *
         * @since       1.0.0
         * @access      public
         * @return      void
         */
        public function render() {

            echo '</fieldset></td></tr>';

            $nonce = wp_create_nonce("redux_{$this->parent->args['opt_name']}_wbc_importer");

            // No errors please
            $defaults = array(
                'id'        => '',
                'url'       => '',
                'width'     => '',
                'height'    => '',
                'thumbnail' => '',
            );

            $this->value = wp_parse_args( $this->value, $defaults );

            $imported = false;
			
            $this->field['wbc_demo_imports'] = apply_filters( "redux/{$this->parent->args['opt_name']}/field/wbc_importer_files", array() );
			
			echo '<tr class="dfd-full-width-options-field dfd-field-offset-top dfd-demo-import-section">';
			echo '<th scope="row">';

				echo '<div class="redux_field_th">';
					echo '<div class="redux-field-info">';
						esc_html_e( 'Ronneby demo importer', 'dfd' );
					echo '</div>';
					echo '<span class="description" id="dfd-import-description">';
						echo '<div class="redux-hint-qtip" style="float:left; font-size: 16px; color:lightgray;" qtip-title="Important notice!" qtip-content="All of your customizations in Theme options will be changed to the settings coming with the demo you install. Please backup your changes before running the importer" data-hasqtip="12">';
							echo '<i class="el el-question"></i>';
						echo '</div>';
						echo apply_filters( 'wbc_importer_description', '<span style="color: #2c2c2c;">'.esc_html__('Note!', 'dfd').' </span>'. esc_html__( 'Works best to import on a new install of WordPress', 'dfd' ) );
					echo '</span>';
				echo '</div>';

			echo '</th>';
			echo '<td>';
			
			echo '<fieldset class="redux-field wbc_importer" data-loading-text="'.esc_attr__('Please wait, demo installation is running', 'dfd').'" data-widgets="'.esc_attr__('Installing Widgets', 'dfd').'" data-options="'.esc_attr__('Installing Options', 'dfd').'" data-content="'.esc_attr__('Installing Content', 'dfd').'" data-done="'.esc_attr__('Done', 'dfd').'">';

			$this->afterImportSuccess();
			
            echo '<div class="theme-browser">'
					. '<div class="themes">';

            if(!empty($this->field['wbc_demo_imports'])) {
				
				$demoInfo = Dfd_Theme_Helpers::demoImporterHelper();
				
                foreach($this->field['wbc_demo_imports'] as $section => $imports) {
                    if ( empty( $imports ) ) {
                        continue;
                    }

                    if (!array_key_exists('imported', $imports)) {
                        $extra_class = 'not-imported';
                        $imported = false;
                        $import_message = esc_html__('Import Demo', 'dfd');
                    } else {
                        $imported = true;
                        $extra_class = 'active imported';
                        $import_message = esc_html__('Demo Imported', 'dfd');
                    }
                    echo '<div class="wrap-importer theme '.$extra_class.'" data-demo-id="'.esc_attr( $section ).'"  data-nonce="'. $nonce .'" id="'. $this->field['id'] .'-custom_imports">';
						echo '<div class="dfd-import-wrap-cover">';

							echo '<div class="theme-screenshot">';
								if(isset( $imports['image'])) {
									$urlPrefix = $this->demo_data_url;
									if($imports['content_file']) {
										$urlPrefix = 'http://dfd.name/check/files/rnb/';
									}
									echo  '<div class="image-inner" style="background-image: url('.esc_url($urlPrefix.$imports['directory'].'/'.$imports['image']).');"></div>';
								}
								if($imported) {
									echo '<div class="wbc-importer-buttons importer-button dfd-imported-badge"></div>';
								}
							echo '</div>';

							$title = $imports['directory'];
							
							if(isset($demoInfo[$imports['directory']]['title']) && !empty($demoInfo[$imports['directory']]['title'])) {
								$title = $demoInfo[$imports['directory']]['title'];
							}

							echo '<h3 class="theme-name">'. esc_html(apply_filters('wbc_importer_directory_title', $title)) .'</h3>';
							
							$plugins_data = '';
							
							if(!empty($demoInfo[$imports['directory']]['plugins'])) {
								$plugins_data .= '<div class="theme-actions" data-plugins="'. urlencode($demoInfo[$imports['directory']]['plugins']) .'">';
							} else {
								$plugins_data .= '<div class="theme-actions">';
							}
							
							echo $plugins_data;
							
							if(false == $imported) {
								echo '<div class="wbc-importer-buttons">'
										. '<span class="button-primary importer-button import-demo-data">' . esc_html__('Import Demo', 'dfd') . '</span>'
									.'</div>';
							} else {
								echo '<div class="wbc-importer-buttons button-primary import-demo-data importer-button wbc-importer-reimport">'
										. '<span>'.esc_html__('Re-','dfd').'</span>'.esc_html__('Import demo', 'dfd')
									.'</div>';
							}
							if(isset($demoInfo[$imports['directory']]['url']) && !empty($demoInfo[$imports['directory']]['url'])) {
								echo '<a href="'.esc_url($demoInfo[$imports['directory']]['url']).'" target="_blank" title="'.esc_attr__('See demo', 'dfd').'" class="dfd-import-see-demo"></a>';
							}
							echo '</div>';
						echo '</div>';
                    echo '</div>';
                }
            } else {
				echo '<div class="no-demo-data-available">';
					echo "<h3>".esc_html__('Unfortunately there is no demo data here.', 'dfd')."</h3>";
					echo '<div class="description">'.esc_html__('You can see this message because fo several reasons. First of all, please make sure that your theme is activated by following ', 'dfd').'<a href="'.esc_url(get_admin_url()).'/admin.php?page=dfd-ronneby" target="_blank">'.esc_html__('this link', 'dfd').'</a>. '.esc_html__('Some kind of technical problem such as server configuration, connection etc. can also cause this issue. Please get in touch with our support if your theme is activated but the issue is still here. Simply submit a ticket by following ', 'dfd').'<a href="http://rnbtheme.com/documentation/support-ticket/" target="_blank">'.esc_html__('this link', 'dfd').'</a> '.esc_html__('and our support will do there best for you to get this sorted.' ,'dfd').'</div>';
				echo '</div>';
            }

            echo '</div>'
			. '</div>';
            echo '</fieldset></td></tr>';
        }
		
		public function afterImportSuccess() {
			if(isset($_GET['success']) && $_GET['success'] == '1' && isset($_GET['demo']) && !empty($_GET['demo'])) {
				echo '<div class="dfd-after-import-success">';
					echo '<h2><span>'.esc_html($_GET['demo']).'</span> '.esc_html__('Demo was successfully installed', 'dfd').'!</h2>';
					echo '<div class="dfd-import-button-wrapper">';
						echo '<a href="'.esc_url(get_home_url()).'" target="_blank" class="button button-primary" title="'.esc_attr__('Preview installed demo', 'dfd').'"><i></i>'.esc_html__('Preview installed demo', 'dfd').'</a>';
					echo '</div>';
					echo '<div class="dfd-import-delimiter">'.esc_html__('or', 'dfd').'</div>';
					echo '<div class="dfd-import-button-wrapper dfd-import-additional-buttons">';
						echo '<div class="button-cover"><a href="//rnbtheme.com/documentation/" target="_blank" class="button" title="'.esc_attr__('Read documentation', 'dfd').'">'.esc_html__('Read documentation', 'dfd').'</a></div>';
						echo '<div class="button-cover"><a href="//www.youtube.com/playlist?list=PLb4J5GIHmy1nfLsOnoMEXochzRfxnZDkT" target="_blank" class="button" title="'.esc_attr__('Watch video tutorials', 'dfd').'">'.esc_html__('Watch video tutorials', 'dfd').'</a></div>';
						echo '<div class="button-cover"><a href="//dfd.name/services/" target="_blank" class="button" title="'.esc_attr__('Order customization', 'dfd').'">'.esc_html__('Order customization', 'dfd').'</a></div>';
					echo '</div>';
				echo '</div>';
			}
		}

        /**
         * Enqueue Function.
         *
         * @since       1.0.0
         * @access      public
         * @return      void
         */
        public function enqueue() {

            $min = Redux_Functions::isMin();

            wp_enqueue_script(
                'redux-field-wbc-importer-js',
                $this->extension_url . '/field_wbc_importer' . $min . '.js',
                array('jquery'),
                time(),
                true
            );
//
//            wp_enqueue_style(
//                'redux-field-wbc-importer-css',
//                $this->extension_url . 'field_wbc_importer.css',
//                time(),
//                true
//            );

        }
    }
}
