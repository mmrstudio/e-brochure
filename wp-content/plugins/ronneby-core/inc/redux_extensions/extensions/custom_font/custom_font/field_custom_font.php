<?php
/**
 * @package     ReduxFramework
 * @author      DFD
 * @version     1.0.0
 */
// Exit if accessed directly
if (!defined('ABSPATH'))
	exit;

// Don't duplicate me!
if (!class_exists('ReduxFramework_custom_font')) {

	/**
	 * Main ReduxFramework_custom_field class
	 *
	 * @since       1.0.0
	 */
	class ReduxFramework_custom_font extends ReduxFramework {

		/**
		 * Field Constructor.
		 *
		 * Required - must call the parent constructor, then assign field and value to vars, and obviously call the render field function
		 *
		 * @since       1.0.0
		 * @access      public
		 * @return      void
		 */
		function __construct($field = array (), $value = '', $parent) {

			$this->parent = $parent;
			$this->field = $field;
			$this->value = $value;

			if (empty($this->extension_dir)) {
				$this->extension_dir = trailingslashit(str_replace('\\', '/', dirname(__FILE__)));
				$this->extension_url = site_url(str_replace(trailingslashit(str_replace('\\', '/', ABSPATH)), '', $this->extension_dir));
			}

			// Set default args for this field to avoid bad indexes. Change this to anything you use.
			$defaults = array (
					'options' => array (""),
					'stylesheet' => 'fff',
					'output' => true,
					'enqueue' => true,
					'enqueue_frontend' => true
			);
			$this->field = wp_parse_args($this->field, $defaults);
			
		}

		/**
		 * Field Render Function.
		 *
		 * Takes the vars and outputs the HTML for the field in the settings
		 *
		 * @since       1.0.0
		 * @access      public
		 * @return      void
		 */
		public function render() {
			$def_values = Custom_Font_Validator::generateDefaultArray($this->field);
//			print_r($def_values);
			$this->value = Custom_Font_Validator::addDefaultFontToArray($def_values, $this->value);
//			$this->value = array_merge($this->value, $def_values);
//			print_r($def_values);
			Custom_Font_Validator::instance()->normalizeFolderFont($this->value);
			$defaults = array (
					'show' => array (
							'title' => true,
							'description' => true,
							'url' => true,
					),
					'content_title' => __('Font', 'dfd')
			);

			$this->field = wp_parse_args($this->field, $defaults);
//			echo '<div class="redux-slides-accordion2" data-new-content-title="' . esc_attr(sprintf(__('Adding new %s', 'dfd'), $this->field['content_title'])) . '">';
			echo '<div class="redux-slides-accordion2" data-new-content-title="">';

			$x = 0;
			$multi = ( isset($this->field['multi']) && $this->field['multi'] ) ? ' multiple="multiple"' : "";
			$jsonArr = array (
					"[zip]" => "application/zip"
			);
			$libFilter = urlencode(json_encode($jsonArr));
			$slides = $this->value;

			$font_list = Custom_Font_Validator::font_list();
			$all_fonts = Custom_Font_Validator::instance()->getLocalFonts();
			if (empty($font_list)) {
				$font_list = array ();
			}
			?>
			<style type="text/css">
			<?php
			// create css for previews
			foreach ($font_list as $f_name => $f_path) {

				echo Custom_Font_Validator::fontface_css_creator($f_path["name"], $f_path["id"]) . '
						#preview_' . $f_path["name"] . ' {	
						font-family: "' . $f_name . '";
					}';
			}
			?>
			</style>
			<?php
			if (empty($slides)) {
				$slides = array ("");
			}
			
			$previewThumbUrl = get_template_directory_uri() . '/assets/img/font-zip.png';
			
			foreach ($slides as $slide) {
				$defaults = array (
						'title' => '',
						'description' => '',
						'sort' => '',
						'url' => '',
						'image' => '',
						'thumb' => '',
						'attachment_id' => '',
						'height' => '',
						'width' => '',
						'select' => array (),
				);
				$slide = wp_parse_args($slide, $defaults);

				if (empty($slide['thumb']) && !empty($slide['attachment_id'])) {
					$img = wp_get_attachment_image_src($slide['attachment_id'], 'full');
					$slide['image'] = $img[0];
					$slide['width'] = $img[1];
					$slide['height'] = $img[2];
				}
				$hide = '';
				if (empty($slide['image'])) {
					$hide = ' hide';
				}

				$font_face = $slide["attachment_id"] ? "Unknown_font" : "";
				$font_face_name = $slide["attachment_id"] ? "Unknown font" : "";
				$preview_text = $slide["attachment_id"] ? "The quick brown fox jumps over the lazy dog" : "";
				foreach ($font_list as $font_key => $font) {
					if ($font["id"] == $slide["attachment_id"]) {
						$font_face = $font["name"];
						$font_face_name = $font_key;
					}
				}

				$hide = '';
				if (empty($slide['image']) || $slide['image'] == '') {
					$hide = ' hide';
				}
				
				echo '<div class="redux-slides-accordion-group">'
					. '<fieldset class="redux-field" data-id="' . $this->field['id'] . '">';
						echo '<div class="dfd-custom-fonts-left">';
							echo '<div class="dfd-redux-custom-fonts-field-heading">';
								echo '<div class="redux-hint-qtip" style="float:left; font-size: 16px; color:lightgray;" qtip-title="Add a new font" qtip-content="Allows you to upload any custom font and then use it in your typography options" data-hasqtip="12">';
									echo '<i class="el el-question"></i>';
								echo '</div>';
								esc_html_e( 'Adding new font', 'dfd' );
							echo '</div>';
						echo '</div>';

						echo '<div class="dfd-custom-fonts-right">';

							echo '<div class="screenshot-wrapper">';
								echo '<div class="screenshot' . $hide . '" data-preview-thumb="'.$previewThumbUrl.'">';
									echo '<a class="of-uploaded-image" href="' . $slide['image'] . '" target="_blank" rel="external">';
//										echo '<img class="redux-slides-image" id="image_image_id_' . $x . '" src="' . $slide['thumb'] . '" alt="'.esc_attr__('Thumb', 'dfd').'" />';
										echo '<img class="redux-slides-image" id="image_image_id_' . $x . '" src="' . $previewThumbUrl . '" alt="'.esc_attr__('Thumb', 'dfd').'" />';
									echo '</a>';
								echo '</div>';
								echo '<div class="redux_slides_add_remove">';

									echo '<span class="button media_upload_font_button" id="add_' . $x . '">' . __('Upload', 'dfd') . '</span>';

									echo '<span class="button remove-image' . $hide . '" id="reset_' . $x . '" rel="' . $slide['attachment_id'] . '">' . __('Remove', 'dfd') . '</span>';

								echo '</div>' . "\n";
								echo '<h3>'
										. '<span class="redux-slides-header">' . $font_face_name . '</span>'
										. '<span  class="redux_upload_file_name"></span>'
									. '</h3>';
							echo '</div>';


							echo '<div id="preview_' . $font_face . '" class="preview-font '.$hide.'">' . $preview_text . '</div>';
							
							echo '<ul id="' . $this->field['id'] . '-ul" class="redux-slides-list">';

								echo '<li>'
										. '<input type="hidden" class="slide-sort" name="' . $this->field['name'] . '[' . $x . '][sort]' . $this->field['name_suffix'] . '" id="' . $this->field['id'] . '-sort_' . $x . '" value="' . $slide['sort'] . '" />'
									. '</li>';
								echo '<li>'
										. '<input type="hidden" class="upload-id" name="' . $this->field['name'] . '[' . $x . '][attachment_id]' . $this->field['name_suffix'] . '" id="' . $this->field['id'] . '-image_id_' . $x . '" value="' . $slide['attachment_id'] . '" />';
									echo '<input type="hidden" class="upload-thumbnail" name="' . $this->field['name'] . '[' . $x . '][thumb]' . $this->field['name_suffix'] . '" id="' . $this->field['id'] . '-thumb_url_' . $x . '" value="' . $slide['thumb'] . '" readonly="readonly" />';
									echo '<input type="hidden" class="library-filter" data-lib-filter="' . $libFilter . '" />';

									echo '<input type="hidden" class="upload" name="' . $this->field['name'] . '[' . $x . '][image]' . $this->field['name_suffix'] . '" id="' . $this->field['id'] . '-image_url_' . $x . '" value="' . $slide['image'] . '" readonly="readonly" />';
									echo '<input type="hidden" class="upload-height" name="' . $this->field['name'] . '[' . $x . '][height]' . $this->field['name_suffix'] . '" id="' . $this->field['id'] . '-image_height_' . $x . '" value="' . $slide['height'] . '" />';
									echo '<input type="hidden" class="upload-width" name="' . $this->field['name'] . '[' . $x . '][width]' . $this->field['name_suffix'] . '" id="' . $this->field['id'] . '-image_width_' . $x . '" value="' . $slide['width'] . '" />'
									. '</li>';
								echo '<li>'
										. '<a href="javascript:void(0);" class="button deletion redux-slides-remove">' . __('Delete', 'dfd') . '</a>'
									. '</li>';
							echo '</ul>'
							. '</div>'
						. '</fieldset>'
					. '</div>';
				$x ++;
			}
			echo '</div>'
				. '<a href="javascript:void(0);" class="button redux-slides-add button-primary" rel-id="' . $this->field['id'] . '-ul" rel-name="' . $this->field['name'] . '[title][]' . $this->field['name_suffix'] . '">' . sprintf(__('Add one more %s', 'dfd'), $this->field['content_title']) . '</a>';
		}

		/**
		 * Enqueue Function.
		 *
		 * If this field requires any scripts, or css define this function and register/enqueue the scripts/css
		 *
		 * @since       1.0.0
		 * @access      public
		 * @return      void
		 */
		public function enqueue() {
			if (function_exists('wp_enqueue_media')) {
				wp_enqueue_media();
			} else {
				wp_enqueue_script('media-upload');
			}

			if ($this->parent->args['dev_mode']) {
				wp_enqueue_style('redux-field-media-css');

				wp_enqueue_style(
						  'redux-field-slides-css', ReduxFramework::$_url . 'inc/fields/slides/field_slides.css', array (), time(), 'all'
				);
			}
			wp_enqueue_style(
					  'redux-field-slides-css1', $this->extension_url . 'field_custom_field.css', array (), time(), 'all'
			);

			wp_enqueue_script(
					  'redux-field-media-js1', $this->extension_url . 'media.js', array ('jquery', 'redux-js'), time(), true
			);

			wp_enqueue_script(
					  'redux-field-slides-js2', $this->extension_url . 'field_slides.js', array ('jquery', 'jquery-ui-core', 'jquery-ui-accordion', 'jquery-ui-sortable', 'redux-field-media-js'), time(), true
			);
		}

		/**
		 * Output Function.
		 *
		 * Used to enqueue to the front-end
		 *
		 * @since       1.0.0
		 * @access      public
		 * @return      void
		 */
		public function output() {

			if ($this->field['enqueue_frontend']) {
				
			}
		}

	}

}
