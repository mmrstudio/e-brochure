<?php
if ( !defined( 'ABSPATH' )) { exit; }

$sale_font_options = $use_sale_google_fonts = $sale_custom_fonts = $sale_bg_color = $link_css = $sale_hover_color = $sale_bg_hover_color = '';
$price_font_options = $use_price_google_fonts = $custom_price_fonts = '';
			
$atts = vc_map_get_attributes( 'woocomposer_grid', $atts );
extract( $atts );

extract(shortcode_atts(array(
	'product_style' => '',
	'module_animation' => '',
),$atts));
$output = '';
$uid = uniqid();

$animate = $animation_data = '';
if ( ! ( $module_animation == '' ) ) {
	$animate        = ' cr-animate-gen';
	$animation_data = ' data-animate-item=".prod-wrap" data-animate-type = "' . esc_attr($module_animation) . '" ';
}

$sale_font_options = _crum_parse_text_shortcode_params($sale_font_options, '', $use_sale_google_fonts, $sale_custom_fonts, true);
if(isset($sale_font_options['style']) && !empty($sale_font_options['style'])) {
	$link_css .= '#woo-grid-'.esc_js($uid).' .onsale {'.$sale_font_options['style'].'}';
}
if(isset($sale_bg_color) && !empty($sale_bg_color)) {
	$link_css .= '#woo-grid-'.esc_js($uid).' .onsale {background: '.$sale_bg_color.';}';
}
if(isset($sale_bg_hover_color) && !empty($sale_bg_hover_color)) {
	$link_css .= '#woo-grid-'.esc_js($uid).' .onsale:hover {background: '.$sale_bg_hover_color.';}';
}
if(isset($sale_hover_color) && !empty($sale_hover_color)) {
	$link_css .= '#woo-grid-'.esc_js($uid).' .onsale:hover {color: '.$sale_hover_color.' !important;}';
}

$price_font_options = _crum_parse_text_shortcode_params($price_font_options, '', $use_price_google_fonts, $custom_price_fonts, true);
if(isset($price_font_options['style']) && !empty($price_font_options['style'])) {
	$link_css .= '#woo-grid-'.esc_js($uid).' .product .woo-title-wrap .price-wrap .price {'.$price_font_options['style'].'}';
	$link_css .= '#woo-grid-'.esc_js($uid).' .product .woo-title-wrap .price-wrap .amount {'.$price_font_options['style'].'}';
}

$output = '<div id="woo-grid-'.esc_attr($uid).'" class="woocomposer_grid woo-product-grid-wrap '.esc_attr($animate).'" '.$animation_data.'>';

require_once(DFD_RONNEBY_PLUGIN_PATH .'inc/vc_custom/dfd_vc_addons/dfd_woo_shortcodes/design/design-loop.php');
$output .= Dfd_Woocommerce_Loop_module($atts);
$output .= '</div>';

if(!empty($link_css)) {
	$output .=	'<script>
					(function($){
						$("head").append("<style>'. esc_js($link_css) .'</style>");
					})(jQuery)
				</script>';
}

echo $output;