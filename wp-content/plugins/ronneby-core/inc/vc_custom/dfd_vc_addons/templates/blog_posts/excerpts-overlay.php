<?php
if ( ! defined( 'ABSPATH' ) ) { exit; }

$content_class = '';
?>
<div class="dfd-blog-loop dfd-blog-posts-module <?php echo esc_attr($el_class) ?>" id="<?php echo esc_attr($uniqid) ?>">
	<div class="dfd-blog-wrap module-eq-height">
		
		<div class="dfd-blog row <?php echo esc_attr($anim_class) ?>" <?php echo $data_atts ?>>
		<?php
			$i = 0;
			while ($wp_query->have_posts()) : $wp_query->the_post();
				$i++;

				$permalink = get_permalink();

				$post_class = 'post';
				
				if($i == 1) {

					?>
					<div class="main-post-wrap seven columns">
						<div class="<?php echo esc_attr($post_class) ?>">
							<?php
								$caption = get_the_title();
								if (has_post_thumbnail()) {
									$thumb = get_post_thumbnail_id();
									$img_src = wp_get_attachment_image_src($thumb, 'full');
									$img_url = (isset($img_src[0]) && !empty($img_src[0])) ? $img_src[0] : get_template_directory_uri() . '/assets/images/no_image_resized_675-450.jpg';
								} else {
									$img_url = get_template_directory_uri() . '/assets/images/no_image_resized_675-450.jpg';
								}
							?>
							<div class="entry-media" style="background-image: url(<?php echo esc_url($img_url); ?>); "></div>
							
							<div class="cover">
								<div class="dfd-vertical-aligned">
								
									<?php
									include(DFD_RONNEBY_PLUGIN_PATH.'inc/vc_custom/dfd_vc_addons/templates/blog_posts/template_parts/heading.php');
									?>

									<?php if($enable_excerpt || $read_more || $share) : ?>
										<div class="content-cover">
											<?php
											if($enable_excerpt && !empty(get_the_excerpt())) {
												if($dropcap_excerpt) {
													$content_class = 'enable-dropcap-excerpt';
												}
												echo '<div class="entry-content '.esc_attr($content_class).'"><p>'.get_the_excerpt().'</p></div>';
											}
											?>
											<?php if($read_more || $share) : ?>
												<div class="dfd-read-share clearfix">
													<?php if($read_more) : ?>
														<div class="read-more-wrap">
															<a href="<?php echo esc_url($permalink) ?>" class="more-button <?php echo esc_attr($read_more_style) ?>" title="<?php __('Read more','dfd') ?>" data-lang="en"><?php echo $read_more_word; ?></a>
														</div>
													<?php endif; ?>
													<?php if($share) : ?>
														<div class="dfd-share-cover dfd-share-<?php echo esc_attr($share_style);  ?>">
															<?php get_template_part('templates/entry-meta/mini','share-blog') ?>
														</div>
													<?php endif; ?>
												</div>
											<?php endif; ?>
										</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					<div class="additional-posts-wrap five columns">
				<?php
				} else {
					?>
						<div class="<?php echo esc_attr($post_class) ?> dfd-additional-post">
							<div class="cover">
								<?php
								$caption = get_the_title();
								if (has_post_thumbnail()) {

									$thumb = get_post_thumbnail_id();
									$img_src = wp_get_attachment_image_src( $thumb, 'full' );
									$img_url = (isset($img_src[0]) && !empty($img_src[0])) ? $img_src[0] : get_template_directory_uri() . '/assets/images/no_image_resized_675-450.jpg';
									
									$image_url    = dfd_aq_resize( $img_src[0], 300, 300, true, true, true );
									if(!$image_url) {
										$image_url = $img_src[0];
									}
									
									$meta = wp_get_attachment_metadata($thumb);
									if(isset($meta['image_meta']['caption']) && $meta['image_meta']['caption'] != '') {
										$caption = $meta['image_meta']['caption'];
									} else if(isset($meta['image_meta']['title']) && $meta['image_meta']['title'] != '') {
										$caption = $meta['image_meta']['title'];
									}

								} else {
									$img_url = get_template_directory_uri() . '/assets/images/no_image_resized_675-450.jpg';
								}
								
								global $dfd_ronneby;
								$thumb_class = $loading_img_src = $image_html = '';
								if(isset($dfd_ronneby['enable_images_lazy_load']) && $dfd_ronneby['enable_images_lazy_load'] == 'on') {
									$thumb_class = ' dfd-img-lazy-load';
									$loading_img_src = "data:image/svg+xml;charset=utf-8,%3Csvg xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg' viewBox%3D'0 0 300 300'%2F%3E";
									$image_html = '<img src="'.$loading_img_src.'" data-src="'.esc_url($image_url).'" width="300" height="300" alt="'.esc_attr($caption).'" />';
								} else {
									$image_html = '<img src="'.esc_url($image_url).'" width="300" height="300" alt="'.esc_attr($caption).'" />';
								}
								
								?>
								<div class="entry-thumb <?php echo $thumb_class; ?>">
									<?php echo $image_html; ?>
								</div>
								<?php 
									if($enable_cat) {
										echo '<div class="dfd-news-categories">';
											get_template_part('templates/entry-meta/mini', 'category-highlighted');
										echo '</div>';
									}
								?>
								<?php include(DFD_RONNEBY_PLUGIN_PATH.'inc/vc_custom/dfd_vc_addons/templates/blog_posts/template_parts/additional-heading.php'); ?>
								<?php if($read_more || $share) : ?>
									<div class="dfd-read-share clearfix">
										<?php if($read_more) : ?>
											<div class="read-more-wrap">
												<a href="<?php echo esc_url($permalink) ?>" class="more-button <?php echo esc_attr($read_more_style) ?>" title="<?php __('Read more','dfd') ?>" data-lang="en"><?php echo $read_more_word; ?></a>
											</div>
										<?php endif; ?>
										<?php if($share) : ?>
											<div class="dfd-share-cover dfd-share-<?php echo esc_attr($share_style);  ?>">
												<?php get_template_part('templates/entry-meta/mini','share-blog') ?>
											</div>
										<?php endif; ?>
									</div>
								<?php endif; ?>
							</div>
						</div>
						<?php
				}
				
			endwhile;
			wp_reset_postdata();
			?>
			</div>
		</div>
	</div>
</div>
