<?php
if ( !defined( 'ABSPATH' )) { exit; }
/*
* Add-on Name: DFD Client Logo for Visual Composer
*/
if( !class_exists('Dfd_Timeline')) {
	
	class Dfd_Timeline {
		
		function __construct() {
			add_action('init', array(&$this, 'dfd_timeline_init'));
			add_shortcode('dfd_timeline', array(&$this, 'dfd_timeline'));
		}
		
		function dfd_timeline_init () {
			if ( function_exists('vc_map') ) {
				//$module_images = DFD_RONNEBY_PLUGIN_URL .'/inc/vc_custom/dfd_vc_addons/admin/img/.../';
				vc_map(
					array(
						'name' => esc_html__('Timeline', 'dfd'),
						'base' => 'dfd_timeline',
						'icon' => 'dfd_timeline dfd_shortcode',
						'category' => esc_html__('Ronneby', 'dfd'),
						'params' => array(
//							array(
//								'heading'			=> esc_html__('Style', 'dfd'),
//								'type'				=> 'radio_image_select',
//								'param_name'		=> 'main_style',
//								'simple_mode'		=> false,
//								'options'			=> array(
//									'style-1'			=> array(
//										'tooltip'			=> esc_attr__('Simple','dfd'),
//										'src'				=> $module_images.'style-1.png'
//									),
//									'style-2'			=> array(
//										'tooltip'			=> esc_attr__('Slide up','dfd'),
//										'src'				=> $module_images.'style-2.png'
//									),
//								),
//							),
							array(
								'type' => 'dfd_radio_advanced',
								'heading' => '<span class="dfd-vc-toolip tooltip-bottom"><i class="dfd-socicon-question-sign"></i><span class="dfd-vc-tooltip-text">' . esc_html__('This option allows to specify the number of timeline items', 'dfd') . '</span></span>' . esc_html__('Columns', 'dfd'),
								'param_name' => 'columns',
								'value' => 5,
								'options' => array(
									esc_html__('1', 'dfd') => 1,
									esc_html__('2', 'dfd') => 2,
									esc_html__('3', 'dfd') => 3,
									esc_html__('4', 'dfd') => 4,
									esc_html__('5', 'dfd') => 5
								),
								'edit_field_class' => 'vc_column vc_col-sm-6',
							),
							array(
								'type' => 'dfd_radio_advanced',
								'heading' => '<span class="dfd-vc-toolip tooltip-bottom"><i class="dfd-socicon-question-sign"></i><span class="dfd-vc-tooltip-text">' . esc_html__('This option allows you to specify the text alignmnt for the timeline conent', 'dfd') . '</span></span>' . esc_html__('Content alignment', 'dfd'),
								'param_name' => 'content_alignment',
								'value' => 'text-center',
								'options' => array(
									esc_html__('Left', 'dfd') => 'text-left',
									esc_html__('Center', 'dfd') => 'text-center',
									esc_html__('Right', 'dfd') => 'text-right'
								),
								'edit_field_class' => 'vc_column vc_col-sm-6',
							),
							array(
								'type' => 'dropdown',
								'heading' => '<span class="dfd-vc-toolip"><i class="dfd-socicon-question-sign"></i><span class="dfd-vc-tooltip-text">' . esc_html__('Choose the appear effect for the element', 'dfd') . '</span></span>' . esc_html__('Animation', 'dfd'),
								'param_name' => 'module_animation',
								'value' => Dfd_Theme_Helpers::module_animation_styles(),
							),
							array(
								'type' => 'textfield',
								'heading' => '<span class="dfd-vc-toolip"><i class="dfd-socicon-question-sign"></i><span class="dfd-vc-tooltip-text">' . esc_html__('Add the unique class name for the element which can be used for custom CSS codes', 'dfd') . '</span></span>' . esc_html__('Custom CSS Class', 'dfd'),
								'param_name' => 'el_class',
							),
//							array(
//								'type'				=> 'dfd_video_link_param',
//								'heading'			=> '<span class="dfd-vc-toolip"><i class="dfd-socicon-question-sign"></i><span class="dfd-vc-tooltip-text">'.esc_html__('Video tutorial and theme documentation article','dfd').'</span></span>'.esc_html__('Tutorials','dfd'),
//								'param_name'		=> 'tutorials',
//								'doc_link'			=> '//nativewptheme.net/support/visual-composer/clients-logos',
//								'video_link'		=> 'https://www.youtube.com/watch?v=NU7LgIuQOc8&feature=youtu.be',
//							),
							array(
								'type' => 'param_group',
								'heading' => esc_html__('Timeline content', 'dfd'),
								'param_name' => 'list_fields',
								'params' => array(
									array(
										'type' => 'textfield',
										'heading' => esc_html__('Date', 'dfd'),
										'param_name' => 'block_date',
										'admin_label' => true,
									),
									array(
										'type' => 'textfield',
										'heading' => esc_html__('Title', 'dfd'),
										'param_name' => 'block_title',
										'admin_label' => true,
									),
									array(
										'type' => 'textfield',
										'heading' => esc_html__('Subtitle', 'dfd'),
										'param_name' => 'block_subtitle',
									),
									array(
										'type' => 'textarea',
										'heading' => esc_html__('Description', 'dfd'),
										'param_name' => 'block_content',
									),
									array(
										'type' => 'dfd_single_checkbox',
										'heading' => '<span class="dfd-vc-toolip"><i class="dfd-socicon-question-sign"></i><span class="dfd-vc-tooltip-text">' . esc_html__('This opition allows you to mark item as completed action in timeline', 'dfd') . '</span></span>' . esc_html__('Completed action', 'dfd'),
										'param_name' => 'completed_action',
										'options' => array(
											'on' => array(
												'on' => esc_attr__('Yes', 'dfd'),
												'off' => esc_attr__('No', 'dfd'),
											),
										),
										'edit_field_class' => 'vc_column vc_col-sm-6',
										'group' => esc_html__('Settings', 'dfd'),
									),
								),
								'group' => esc_html__('Content', 'dfd'),
							),
							array(
								'type' => 'colorpicker',
								'heading' => '<span class="dfd-vc-toolip tooltip-bottom"><i class="dfd-socicon-question-sign"></i><span class="dfd-vc-tooltip-text">' . esc_html__('This option allows you to define the color for the completed timeline actions', 'dfd') . '</span></span>' . esc_html__('Main line color', 'dfd'),
								'param_name' => 'main_color',
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'group' => esc_html__('Settings', 'dfd'),
							),
							array(
								'type' => 'colorpicker',
								'heading' => '<span class="dfd-vc-toolip tooltip-bottom"><i class="dfd-socicon-question-sign"></i><span class="dfd-vc-tooltip-text">' . esc_html__('This option allows you to define the color for the incompleted timeline actions', 'dfd') . '</span></span>' . esc_html__('Back line color', 'dfd'),
								'param_name' => 'back_color',
								'edit_field_class' => 'vc_column vc_col-sm-6 no-top-padding',
								'group' => esc_html__('Settings', 'dfd'),
							),
							array(
								'type' => 'dfd_param_heading',
								'text' => esc_html__('Date typography', 'dfd'),
								'param_name' => 'date_t_heading',
								'class' => 'ult-param-heading',
								'edit_field_class' => 'dfd-heading-param-wrapper no-top-margin vc_column vc_col-sm-12',
								'group' => esc_attr__('Typography', 'dfd'),
							),
							array(
								'type' => 'dfd_font_container_param',
								'param_name' => 'date_font_options',
								'settings' => array(
									'fields' => array(
										'tag' => 'div',
										'font_size',
										'letter_spacing',
										'line_height',
										'color',
										'font_style'
									),
								),
								'group' => esc_attr__('Typography', 'dfd'),
							),
							array(
								'type' => 'dfd_single_checkbox',
								'heading' => '<span class="dfd-vc-toolip"><i class="dfd-socicon-question-sign"></i><span class="dfd-vc-tooltip-text">' . esc_html__('Allows you to use custom Google font', 'dfd') . '</span></span>' . esc_html__('Custom font family', 'dfd'),
								'param_name' => 'use_date_google_fonts',
								'options' => array(
									'yes' => array(
										'yes' => esc_attr__('Yes', 'dfd'),
										'no' => esc_attr__('No', 'dfd'),
									),
								),
								'group' => esc_html__('Typography', 'dfd'),
							),
							array(
								'type' => 'google_fonts',
								'param_name' => 'custom_date_fonts',
								'settings' => array(
									'fields' => array(
										'font_family_description' => esc_html__('Select font family.', 'dfd'),
										'font_style_description' => esc_html__('Select font style.', 'dfd'),
									),
								),
								'edit_field_class' => 'vc_column vc_col-sm-12 no-border-bottom',
								'dependency' => array('element' => 'use_date_google_fonts', 'value' => 'yes'),
								'group' => esc_attr__('Typography', 'dfd'),
							),
							array(
								'type' => 'dfd_param_heading',
								'text' => esc_html__('Title typography', 'dfd'),
								'param_name' => 'title_t_heading',
								'class' => 'ult-param-heading',
								'edit_field_class' => 'dfd-heading-param-wrapper no-top-margin vc_column vc_col-sm-12',
								'group' => esc_attr__('Typography', 'dfd'),
							),
							array(
								'type' => 'dfd_font_container_param',
								'param_name' => 'title_font_options',
								'settings' => array(
									'fields' => array(
										'tag' => 'div',
										'font_size',
										'letter_spacing',
										'line_height',
										'color',
										'font_style'
									),
								),
								'group' => esc_attr__('Typography', 'dfd'),
							),
							array(
								'type' => 'dfd_single_checkbox',
								'heading' => '<span class="dfd-vc-toolip"><i class="dfd-socicon-question-sign"></i><span class="dfd-vc-tooltip-text">' . esc_html__('Allows you to use custom Google font', 'dfd') . '</span></span>' . esc_html__('Custom font family', 'dfd'),
								'param_name' => 'use_google_fonts',
								'options' => array(
									'yes' => array(
										'yes' => esc_attr__('Yes', 'dfd'),
										'no' => esc_attr__('No', 'dfd'),
									),
								),
								'group' => esc_html__('Typography', 'dfd'),
							),
							array(
								'type' => 'google_fonts',
								'param_name' => 'custom_fonts',
								'settings' => array(
									'fields' => array(
										'font_family_description' => esc_html__('Select font family.', 'dfd'),
										'font_style_description' => esc_html__('Select font style.', 'dfd'),
									),
								),
								'edit_field_class' => 'vc_column vc_col-sm-12 no-border-bottom',
								'dependency' => array('element' => 'use_google_fonts', 'value' => 'yes'),
								'group' => esc_attr__('Typography', 'dfd'),
							),
							array(
								'type' => 'dfd_param_heading',
								'text' => esc_html__('Subtitle typography', 'dfd'),
								'param_name' => 'subtitle_t_heading',
								'class' => 'ult-param-heading',
								'edit_field_class' => 'dfd-heading-param-wrapper vc_column vc_col-sm-12',
								'group' => esc_html__('Typography', 'dfd'),
							),
							array(
								'type' => 'dfd_font_container_param',
								'param_name' => 'subtitle_font_options',
								'settings' => array(
									'fields' => array(
										'tag' => 'div',
										'font_size',
										'letter_spacing',
										'line_height',
										'color',
										'font_style'
									),
								),
								'edit_field_class' => 'vc_column vc_col-sm-12 no-border-bottom',
								'group' => esc_html__('Typography', 'dfd'),
							),
							array(
								'type' => 'dfd_single_checkbox',
								'heading' => '<span class="dfd-vc-toolip"><i class="dfd-socicon-question-sign"></i><span class="dfd-vc-tooltip-text">' . esc_html__('Allows you to use custom Google font', 'dfd') . '</span></span>' . esc_html__('Custom font family', 'dfd'),
								'param_name' => 'use_sbttl_google_fonts',
								'options' => array(
									'yes' => array(
										'yes' => esc_attr__('Yes', 'dfd'),
										'no' => esc_attr__('No', 'dfd'),
									),
								),
								'group' => esc_html__('Typography', 'dfd'),
							),
							array(
								'type' => 'google_fonts',
								'param_name' => 'custom_sbttl_fonts',
								'settings' => array(
									'fields' => array(
										'font_family_description' => esc_html__('Select font family.', 'dfd'),
										'font_style_description' => esc_html__('Select font style.', 'dfd'),
									),
								),
								'edit_field_class' => 'vc_column vc_col-sm-12 no-border-bottom',
								'dependency' => array('element' => 'use_sbttl_google_fonts', 'value' => 'yes'),
								'group' => esc_attr__('Typography', 'dfd'),
							),
							array(
								'type' => 'dfd_param_heading',
								'text' => esc_html__('Description typography', 'dfd'),
								'param_name' => 'content_t_heading',
								'class' => 'ult-param-heading',
								'edit_field_class' => 'dfd-heading-param-wrapper vc_column vc_col-sm-12',
								'group' => esc_attr__('Typography', 'dfd'),
							),
							array(
								'type' => 'dfd_font_container_param',
								'param_name' => 'dscrp_font_options',
								'settings' => array(
									'fields' => array(
										'font_size',
										'letter_spacing',
										'line_height',
										'color',
										'font_style'
									),
								),
								'group' => esc_attr__('Typography', 'dfd'),
							),
							array(
								'type' => 'dfd_single_checkbox',
								'heading' => '<span class="dfd-vc-toolip"><i class="dfd-socicon-question-sign"></i><span class="dfd-vc-tooltip-text">' . esc_html__('Allows you to use custom Google font', 'dfd') . '</span></span>' . esc_html__('Custom font family', 'dfd'),
								'param_name' => 'use_dscrp_google_fonts',
								'options' => array(
									'yes' => array(
										'yes' => esc_attr__('Yes', 'dfd'),
										'no' => esc_attr__('No', 'dfd'),
									),
								),
								'group' => esc_html__('Typography', 'dfd'),
							),
							array(
								'type' => 'google_fonts',
								'param_name' => 'custom_dscrp_fonts',
								'settings' => array(
									'fields' => array(
										'font_family_description' => esc_html__('Select font family.', 'dfd'),
										'font_style_description' => esc_html__('Select font style.', 'dfd'),
									),
								),
								'edit_field_class' => 'vc_column vc_col-sm-12 no-border-bottom',
								'dependency' => array('element' => 'use_dscrp_google_fonts', 'value' => 'yes'),
								'group' => esc_attr__('Typography', 'dfd'),
							),
							array(
								'type' => 'dfd_radio_advanced',
								'heading' => '<span class="dfd-vc-toolip tooltip-bottom"><i class="dfd-socicon-question-sign"></i><span class="dfd-vc-tooltip-text">' . esc_html__('This option allows you to define number of columns for the screen width 1024-1279', 'dfd') . '</span></span>' . esc_html__('Columns for width 1024-1279', 'dfd'),
								'param_name' => 'desktop_columns',
								'value' => 5,
								'options' => array(
									esc_html__('1', 'dfd') => 1,
									esc_html__('2', 'dfd') => 2,
									esc_html__('3', 'dfd') => 3,
									esc_html__('4', 'dfd') => 4,
									esc_html__('5', 'dfd') => 5
								),
								'group' => esc_html__('Responcive', 'dfd'),
							),
							array(
								'type' => 'dfd_radio_advanced',
								'heading' => '<span class="dfd-vc-toolip tooltip-bottom"><i class="dfd-socicon-question-sign"></i><span class="dfd-vc-tooltip-text">' . esc_html__('This option allows you to define number of columns for the screen width 800-1023', 'dfd') . '</span></span>' . esc_html__('Columns for width 1024-1279', 'dfd'),
								'param_name' => 'tablet_columns',
								'value' => 5,
								'options' => array(
									esc_html__('1', 'dfd') => 1,
									esc_html__('2', 'dfd') => 2,
									esc_html__('3', 'dfd') => 3,
									esc_html__('4', 'dfd') => 4,
									esc_html__('5', 'dfd') => 5
								),
								'group' => esc_html__('Responcive', 'dfd'),
							),
							array(
								'type' => 'dfd_radio_advanced',
								'heading' => '<span class="dfd-vc-toolip tooltip-bottom"><i class="dfd-socicon-question-sign"></i><span class="dfd-vc-tooltip-text">' . esc_html__('This option allows you to define number of columns for the screen width less than 800px', 'dfd') . '</span></span>' . esc_html__('Columns for less than 800', 'dfd'),
								'param_name' => 'mobile_columns',
								'value' => 5,
								'options' => array(
									esc_html__('1', 'dfd') => 1,
									esc_html__('2', 'dfd') => 2,
									esc_html__('3', 'dfd') => 3,
									esc_html__('4', 'dfd') => 4,
									esc_html__('5', 'dfd') => 5
								),
								'group' => esc_html__('Responcive', 'dfd'),
							),
						),
					)
				);
			}
		}
		
		function dfd_timeline($atts, $content = null) {
			$module_animation = $el_class = $list_fields = $output = $css_rules = '';
			$main_color = $back_color = $date_font_options = '';
			$use_date_google_fonts = $custom_date_fonts = $subtitle_font_options = '';
			$title_font_options = $use_google_fonts = $custom_fonts = '';
			$use_sbttl_google_fonts = $custom_sbttl_fonts = $dscrp_font_options = '';
			$use_dscrp_google_fonts = $custom_dscrp_fonts = $date_typography = '';
			$columns = $desktop_columns = $tablet_columns = $mobile_columns = '';
			$item_html = $data_attr = $content_alignment = '';

			$atts = vc_map_get_attributes('dfd_timeline', $atts);
			extract($atts);

			wp_enqueue_script('dfd-timeline');	
			
			$uniqid = uniqid('dfd-timeline-').'-'.rand(1,9999);
			
			$el_class = ' number-col-'.$columns;
			$el_class = ' '.$content_alignment;

			if(!($module_animation == '')) {
				$el_class .= ' cr-animate-gen';
				$data_attr .= ' data-animate-item=".timeline__content" data-animate-type = "'.$module_animation.'"';
			}
			
			$data_attr .= ' data-columns="'.(int)$columns.'"';
			$data_attr .= ' data-columns-desktop="'.(int)$desktop_columns.'"';
			$data_attr .= ' data-columns-tablet="'.(int)$tablet_columns.'"';
			$data_attr .= ' data-columns-mobile="'.(int)$mobile_columns.'"';
			
			$date_typography = _crum_parse_text_shortcode_params($date_font_options, '', $use_date_google_fonts, $custom_date_fonts, true);
			$title_typography = _crum_parse_text_shortcode_params($title_font_options, '', $use_google_fonts, $custom_fonts, true);
			$subtitle_typography = _crum_parse_text_shortcode_params($subtitle_font_options, '', $use_sbttl_google_fonts, $custom_sbttl_fonts, true);
			$description_typography = _crum_parse_text_shortcode_params($dscrp_font_options, '', $use_dscrp_google_fonts, $custom_dscrp_fonts, true);
			
			if(isset($date_typography['style']) && !empty($date_typography['style'])) {
				$css_rules .= '#'.esc_js($uniqid).' .dfd-timeline-date {'.esc_js($date_typography['style']).'}';
			}
			if(isset($title_typography['style']) && !empty($title_typography['style'])) {
				$css_rules .= '#'.esc_js($uniqid).' .dfd-timeline-title {'.esc_js($title_typography['style']).'}';
			}
			if(isset($subtitle_typography['style']) && !empty($subtitle_typography['style'])) {
				$css_rules .= '#'.esc_js($uniqid).' .dfd-timeline-subtitle {'.esc_js($subtitle_typography['style']).'}';
			}
			if(isset($description_typography['style']) && !empty($description_typography['style'])) {
				$css_rules .= '#'.esc_js($uniqid).' .dfd-timeline-description {'.esc_js($description_typography['style']).'}';
			}
			if(isset($back_color) && !empty($back_color)) {
				$css_rules .= '#'.esc_js($uniqid).'.timeline--horizontal .timeline__item .timeline__item__inner:before, #'.esc_js($uniqid).'.timeline--horizontal .timeline__item .timeline__item__inner:after, #'.esc_js($uniqid).' .timeline__item:before, #'.esc_js($uniqid).' .timeline__item:after {background: '.esc_js($back_color).';}';
			}
			if(isset($main_color) && !empty($main_color)) {
				$css_rules .= '#'.esc_js($uniqid).'.timeline--horizontal .timeline__item.completed .timeline__item__inner:before, #'.esc_js($uniqid).' .timeline__item.completed:before, #'.esc_js($uniqid).' .timeline__item.completed:after {background: '.esc_js($main_color).';}';
				$css_rules .= '#'.esc_js($uniqid).' .timeline-nav-button:hover:before {color: '.esc_js($main_color).';}';
			}
			
			if(isset($list_fields) && !empty($list_fields) && function_exists('vc_param_group_parse_atts')) {
				$list_fields = (array) vc_param_group_parse_atts($list_fields);

				foreach($list_fields as $field) {
					$item_class = '';
					if(isset($field['completed_action']) && $field['completed_action'] == 'on') {
						$item_class .= 'completed';
					}
					
					$item_html .= '<div class="timeline__item '.esc_attr($item_class).'">';
						$item_html .= '<div class="timeline__content">';
							if(isset($field['block_date']) && !empty($field['block_date'])) {
								$item_html .= '<'.$date_typography['tag'].' class="dfd-timeline-date box-name">'.esc_attr($field['block_date']).'</'.$date_typography['tag'].'>';
							}
							if(isset($field['block_title']) && !empty($field['block_title'])) {
								$item_html .= '<'.$title_typography['tag'].' class="dfd-timeline-title box-name">'.esc_attr($field['block_title']).'</'.$title_typography['tag'].'>';
							}
							if(isset($field['block_subtitle']) && !empty($field['block_subtitle'])) {
								$item_html .= '<'.$subtitle_typography['tag'].' class="dfd-timeline-subtitle subtitle">'.esc_attr($field['block_subtitle']).'</'.$subtitle_typography['tag'].'>';
							}
							if(isset($field['block_content']) && !empty($field['block_content'])) {
								$item_html .= '<'.$description_typography['tag'].' class="dfd-timeline-description">'.esc_attr($field['block_content']).'</'.$description_typography['tag'].'>';
							}
						$item_html .= '</div>';
					$item_html .= '</div>';
				}
					
				$output .= '<div id="'.esc_attr($uniqid).'" class="dfd-timeline-wrap timeline '.esc_attr($el_class).'" '.$data_attr.'>';
					$output .= '<div class="timeline__wrap">';
						$output .= '<div class="timeline__items">';
							$output .= $item_html;
						$output .= '</div>';
					$output .= '</div>';

					if($css_rules != '') {
						$output .= '<script type="text/javascript">'
									. '(function($) {'
										. '$("head").append("<style>'.$css_rules.'</style>");'
									. '})(jQuery);'
								. '</script>';
					}

				$output .= '</div>';
			}

			return $output;
		}
	}
}

if ( class_exists( 'Dfd_Timeline' ) ) {
	$Dfd_Timeline = new Dfd_Timeline;
}