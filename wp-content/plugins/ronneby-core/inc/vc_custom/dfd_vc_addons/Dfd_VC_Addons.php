<?php
if ( ! defined( 'ABSPATH' ) ) { exit; }
if(!class_exists('Dfd_VC_Addons')) {
	
	// plugin class
	class Dfd_VC_Addons {
		var $paths = array();
		var $module_dir;
		var $woo_module_dir;
		var $params_dir;
		var $admin_css;
		var $vc_template_dir;
		function __construct() {
			$this->vc_template_dir = DFD_RONNEBY_PLUGIN_PATH .'inc/vc_custom/dfd_vc_addons/vc_templates/';
			$this->module_dir = DFD_RONNEBY_PLUGIN_PATH .'inc/vc_custom/dfd_vc_addons/modules/';
			$this->old_module_dir = DFD_RONNEBY_PLUGIN_PATH .'inc/vc_custom/dfd_vc_addons/old_modules/';
			$this->woo_module_dir = DFD_RONNEBY_PLUGIN_PATH .'inc/vc_custom/dfd_vc_addons/dfd_woo_shortcodes/modules/';
			$this->params_dir = DFD_RONNEBY_PLUGIN_PATH .'inc/vc_custom/dfd_vc_addons/params/';
			$this->admin_css = DFD_RONNEBY_PLUGIN_URL .'inc/vc_custom/dfd_vc_addons/admin/css/';
			$this->paths = wp_upload_dir();
			$this->paths['fonts'] 	= 'smile_fonts';
			$this->paths['fonturl'] = set_url_scheme(trailingslashit($this->paths['baseurl']).$this->paths['fonts']);
			add_action('after_setup_theme',array($this,'dfd_vc_init'), 100);
			add_action('admin_enqueue_scripts',array($this,'admin_assets'), 100);
			add_action('wp_enqueue_scripts',array($this,'load_assets'), 100);
		}
		
		function dfd_vc_init() {
			global $dfd_ronneby;
			
			if(class_exists('Ultimate_VC_Addons')) {
				$this->remove_default_addons();
				update_option('ultimate_row', false);
				foreach(glob($this->old_module_dir."*.php") as $old_module) {
					if(substr_count($old_module, 'Dfd_Override_Parallax') > 0 || substr_count($old_module, 'Ultimate_') > 0) {
						require_once($old_module);
					}
				}
			}
			
			if(!isset($dfd_ronneby['disable_ult_addons']) || $dfd_ronneby['disable_ult_addons'] != 'disable') {
				foreach(glob($this->old_module_dir."*.php") as $old_module) {
					if(substr_count($old_module, 'Ultimate_') === 0) {
						require_once($old_module);
					}
				}
			}
			
			require_once(DFD_RONNEBY_PLUGIN_PATH .'inc/vc_custom/dfd_vc_addons/params.php');
			foreach(glob($this->params_dir."*.php") as $param) {
				require_once($param);
			}
			
			foreach(glob($this->module_dir."*.php") as $module) {
				require_once($module);
			}
			
			if(defined('WOOCOMMERCE_VERSION')) {
				if(class_exists('WooCommerce')){
					require_once( DFD_RONNEBY_PLUGIN_PATH .'inc/vc_custom/dfd_vc_addons/dfd_woo_shortcodes/dfd_woocommerce.php');
				}
				if(version_compare( '2.1.0', WOOCOMMERCE_VERSION, '<' )) {
					foreach(glob($this->woo_module_dir.'*.php') as $module) {
						if(substr_count($module, 'shortcode_') > 0) {
							require_once($module);
						}
					}
				}
			}
		}
		
		function admin_assets($hook) {
			if($hook == "post.php" || $hook == "post-new.php" || $hook == "edit.php"){
				wp_enqueue_style('dfd_addons_admin_css',$this->admin_css.'admin_css.css');

				wp_deregister_style('bsf-dfd');
				wp_dequeue_style('bsf-dfd');
				
				$fonts = get_option('smile_fonts');
				if($fonts && is_array($fonts)) {
					foreach($fonts as $font => $info) {
						if($font != 'dfd') {
							if(strpos($info['style'], 'http://' ) !== false) {
								wp_enqueue_style('dfd-'.$font,$info['style']);
							} else {
								wp_enqueue_style('dfd-'.$font,trailingslashit($this->paths['fonturl']).$info['style']);
							}
						}
					}
				}
			}
		}
		
		function load_assets() {
			wp_deregister_style('bsf-dfd');
			wp_dequeue_style('bsf-dfd');
			$fonts = get_option('smile_fonts');
			if($fonts && is_array($fonts)) {
				foreach($fonts as $font => $info) {
					if($font != 'dfd') {
						$style_url = $info['style'];
						if(strpos($style_url, 'http://' ) !== false) {
							wp_enqueue_style('dfd-'.$font,$info['style']);
						} else {
							wp_enqueue_style('dfd-'.$font,trailingslashit($this->paths['fonturl']).$info['style']);
						}
					}
				}
			}
		}
		
		function remove_default_addons() {
			$addons = array(
				array(
					'class' => 'AIO_Icons_Box',
					'action' => 'icon_box_init',
					'shortcode' => 'bsf-info-box',
				),
				array(
					'class' => 'Ultimate_Headings',
					'action' => 'ultimate_headings_init',
					'shortcode' => 'ultimate_heading',
				),
				array(
					'class' => 'Ultimate_Icons',
					'action' => 'ultimate_icon_init',
					'shortcode' => 'ultimate_icons',
				),
				array(
					'class' => 'Ultimate_Icons',
					'action' => 'ultimate_icon_init',
					'shortcode' => 'single_icon_shortcode',
				),
				array(
					'class' => 'AIO_Just_Icon',
					'action' => 'just_icon_init',
					'shortcode' => 'just_icon',
				),
				array(
					'class' => 'Ultimate_Carousel',
					'action' => 'init_carousel_addon',
					'shortcode' => 'ultimate_carousel',
				),
				array(
					'class' => 'Ultimate_Headings',
					'action' => 'ultimate_headings_init',
					'shortcode' => 'ultimate_heading',
				),
				array(
					'class' => 'Ultimate_Info_Banner',
					'action' => 'banner_init',
					'shortcode' => 'ultimate_info_banner',
				),
				array(
					'class' => 'AIO_Info_list',
					'action' => 'add_info_list',
					'shortcode' => 'info_list',
				),
				array(
					'class' => 'AIO_Info_list',
					'action' => 'add_info_list',
					'shortcode' => 'info_list_item',
				),
				array(
					'class' => 'Ultimate_List_Icon',
					'action' => 'list_icon_init',
					'shortcode' => 'ultimate_icon_list',
				),
				array(
					'class' => 'Ultimate_List_Icon',
					'action' => 'list_icon_init',
					'shortcode' => 'ultimate_icon_list_item',
				),
				array(
					'class' => 'Ultimate_Modals',
					'action' => 'ultimate_modal_init',
					'shortcode' => 'ultimate_modal',
				),
				array(
					'class' => 'Ultimate_Google_Maps',
					'action' => 'google_maps_init',
					'shortcode' => 'ultimate_google_map',
				),
				array(
					'class' => 'WooComposer_GridView',
					'action' => 'woocomposer_init_grid',
					'shortcode' => 'woocomposer_grid',
					'woo' => true
				),
				array(
					'class' => 'WooComposer_ViewList',
					'action' => 'woocomposer_init_grid',
					'shortcode' => 'woocomposer_list',
					'woo' => true
				),
				array(
					'class' => 'WooComposer_ViewProduct',
					'action' => 'WooComposer_Init_Product',
					'shortcode' => 'woocomposer_product',
					'woo' => true
				),
			);
			foreach($addons as $addon) {
				if(class_exists($addon['class'])) {
					if(isset($GLOBALS['shortcode_tags'][$addon['shortcode']][0])) {
						if(isset($addon['woo'])) {
							remove_action('admin_init', array($GLOBALS['shortcode_tags'][$addon['shortcode']][0], $addon['action']), 9999);
						} else {
							remove_action('init', array($GLOBALS['shortcode_tags'][$addon['shortcode']][0], $addon['action']), 999);
						}
						remove_shortcode($addon['shortcode']);
					}
				}
			}
		}
	}
	new Dfd_VC_Addons;
}