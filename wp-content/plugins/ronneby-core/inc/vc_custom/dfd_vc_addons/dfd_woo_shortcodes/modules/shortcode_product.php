<?php
if ( ! defined( 'ABSPATH' ) ) { exit; }
/*
@Module: Single Product view
@Since: 1.0
@Package: WooComposer
*/
if(!class_exists('Dfd_Woo_Sinle_Product')){
	class Dfd_Woo_Sinle_Product {
		
		function __construct(){
			if(!is_admin()) {
				add_action('init', array($this, 'WooComposer_Init_Product'));
			}
			add_action('admin_init', array($this, 'WooComposer_Init_Product'));
			add_shortcode('woocomposer_product', array($this, 'WooComposer_Product'));
		} 
		
		function WooComposer_Init_Product(){
			if(function_exists('vc_map')){
				global $dfd_ronneby;
				vc_map(
					array(
						'name' => __('Single Product', 'dfd'),
						'base' => 'woocomposer_product',
						'icon' => 'woocomposer_product dfd_shortcode',
						'category' => __('WooComposer', 'dfd'),
						'description' => 'Display single product from list',
						'controls' => 'full',
						'show_settings_on_create' => true,
						'params' => array(
							array(
								'type' => 'dfd_param_heading',
								'text' => esc_html__('General settings', 'dfd'),
								'param_name' => 'general_heading',
								'edit_field_class' => 'ult-param-heading-wrapper no-top-margin vc_column vc_col-sm-12',
								'group' => esc_html__('General', 'dfd'),
							),
							array(
								'type' => 'radio_image_select',
								'heading' => __('Select Product Style', 'dfd'),
								'param_name' => 'product_style',
								'admin_label' => true,
								'simple_mode' => false,
								'options' => array(
									'style-1' => array(
										'tooltip' => esc_attr__('Simple product', 'dfd'),
										'src' => DFD_RONNEBY_PLUGIN_URL . 'inc/vc_custom/dfd_vc_addons/admin/img/woo_single/style-1.png'
									),
									'style-2' => array(
										'tooltip' => esc_attr__('Full width image hover description', 'dfd'),
										'src' => DFD_RONNEBY_PLUGIN_URL . 'inc/vc_custom/dfd_vc_addons/admin/img/woo_single/style-2.png'
									),
								),
								'group' => esc_html__('General', 'dfd'),
							),
							array(
								'type' => 'radio_image_post_select',
								'heading' => __('Select Product', 'dfd'),
								'param_name' => 'product_id',
								'post_type' => 'product',
								'css' => array(
									'width' => '50px',
									'height' => '50px',
									'background-repeat' => 'repeat',
									'background-size' => 'cover'
								),
								'show_default' => false,
								'group' => esc_html__('General', 'dfd'),
							),
							array(
								'type' => 'textfield',
								'heading' => __('Extra class name', 'dfd'),
								'param_name' => 'el_class',
								'description' => __('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'dfd'),
								'group' => esc_html__('General', 'dfd'),
							),
							array(
								'type' => 'dropdown',
								'heading' => __('Animation', 'dfd'),
								'param_name' => 'module_animation',
								'value' => Dfd_Theme_Helpers::module_animation_styles(),
								'group' => esc_html__('General', 'dfd'),
							),
							array(
								'type' => 'dfd_param_heading',
								'text' => esc_html__('Image settings', 'dfd'),
								'param_name' => 'image_heading',
								'edit_field_class' => 'ult-param-heading-wrapper no-top-margin vc_column vc_col-sm-12',
								'group' => esc_html__('Image Settings', 'dfd'),
							),
							array(
								'type' => 'dropdown',
								'heading' => __('Enable product image', 'dfd'),
								'param_name' => 'enable_product_image',
								'value' => array(
									__('No', 'dfd') => '',
									__('Yes', 'dfd') => 'yes',
								),
								'group' => esc_html__('Image Settings', 'dfd'),
							),
							array(
								'type' => 'dropdown',
								'heading' => __('Select Product Image', 'dfd'),
								'param_name' => 'image_selector',
								'admin_label' => true,
								'value' => array(
									__('Product thumbnail', 'dfd') => 'thumb',
									__('Custom uploaded image', 'dfd') => 'custom_image',
								),
								'dependency' => Array('element' => 'enable_product_image', 'value' => array('yes')),
								'group' => esc_html__('Image Settings', 'dfd'),
							),
							array(
								'type' => 'attach_image',
								'heading' => __('Custom product image', 'dfd'),
								'param_name' => 'custom_image',
								'description' => __('Upload the custom product image', 'dfd'),
								'dependency' => Array('element' => 'image_selector', 'value' => array('custom_image')),
								'group' => esc_html__('Image Settings', 'dfd'),
							),
							array(
								'type' => 'number',
								'heading' => __('Image width', 'dfd'),
								'param_name' => 'image_width',
								'min' => 0,
								'max' => 1920,
								'suffix' => 'px',
								'dependency' => Array('element' => 'enable_product_image', 'value' => array('yes')),
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'group' => esc_html__('Image Settings', 'dfd'),
							),
							array(
								'type' => 'number',
								'heading' => __('Image height', 'dfd'),
								'param_name' => 'image_height',
								'min' => 0,
								'max' => 1920,
								'suffix' => 'px',
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'dependency' => Array('element' => 'enable_product_image', 'value' => array('yes')),
								'group' => esc_html__('Image Settings', 'dfd'),
							),
							array(
								'type' => 'dfd_param_heading',
								'text' => esc_html__('Content settings', 'dfd'),
								'param_name' => 'content_heading',
								'edit_field_class' => 'ult-param-heading-wrapper no-top-margin vc_column vc_col-sm-12',
								'group' => esc_html__('Content', 'dfd'),
							),
							array(
								'type' => 'dfd_single_checkbox',
								'heading' => __('Product Title', 'dfd'),
								'param_name' => 'enable_title',
								'value' => 'yes',
								'options' => array(
									'yes' => array(
										'on' => 'Yes',
										'off' => 'No',
									),
								),
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'group' => esc_html__('Content', 'dfd'),
							),
							array(
								'type' => 'dfd_single_checkbox',
								'heading' => __('Subtitle', 'dfd'),
								'param_name' => 'enable_cat_tag',
								'value' => 'yes',
								'options' => array(
									'yes' => array(
										'on' => 'Yes',
										'off' => 'No',
									),
								),
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'group' => esc_html__('Content', 'dfd'),
							),
							array(
								'type' => 'dfd_single_checkbox',
								'heading' => __('Rating', 'dfd'),
								'param_name' => 'enable_rating',
								'value' => 'yes',
								'options' => array(
									'yes' => array(
										'on' => 'Yes',
										'off' => 'No',
									),
								),
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'group' => esc_html__('Content', 'dfd'),
							),
							array(
								'type' => 'dfd_single_checkbox',
								'heading' => __('Price', 'dfd'),
								'param_name' => 'enable_price',
								'value' => 'yes',
								'options' => array(
									'yes' => array(
										'on' => 'Yes',
										'off' => 'No',
									),
								),
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'group' => esc_html__('Content', 'dfd'),
							),
							array(
								'type' => 'dfd_single_checkbox',
								'heading' => __('Description', 'dfd'),
								'param_name' => 'enable_description',
								'value' => 'yes',
								'options' => array(
									'yes' => array(
										'on' => 'Yes',
										'off' => 'No',
									),
								),
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'group' => esc_html__('Content', 'dfd'),
							),
							array(
								'type' => 'dfd_single_checkbox',
								'heading' => __('Add to Cart', 'dfd'),
								'param_name' => 'enable_add_to_cart',
								'value' => 'yes',
								'options' => array(
									'yes' => array(
										'on' => 'Yes',
										'off' => 'No',
									),
								),
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'group' => esc_html__('Content', 'dfd'),
							),
							array(
								'type' => 'dfd_single_checkbox',
								'heading' => __('Wishlist', 'dfd'),
								'param_name' => 'enable_wishlist',
								'value' => 'yes',
								'options' => array(
									'yes' => array(
										'on' => 'Yes',
										'off' => 'No',
									),
								),
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'group' => esc_html__('Content', 'dfd'),
							),
							array(
								'type' => 'dfd_single_checkbox',
								'heading' => __('Lightbox', 'dfd'),
								'param_name' => 'enable_quick_view',
								'value' => 'yes',
								'options' => array(
									'yes' => array(
										'on' => 'Yes',
										'off' => 'No',
									),
								),
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'group' => esc_html__('Content', 'dfd'),
							),
							array(
								'type' => 'dfd_single_checkbox',
								'heading' => __('Show Add to cart without hover', 'dfd'),
								'param_name' => 'enable_button_default',
								'value' => 'yes',
								'options' => array(
									'yes' => array(
										'on' => 'Yes',
										'off' => 'No',
									),
								),
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'dependency' => array('element' => 'product_style', 'value' => array('style-1')),
								'group' => esc_html__('Content', 'dfd'),
							),
							array(
								'type' => 'number',
								'heading' => __('Description limit (words)', 'dfd'),
								'param_name' => 'desc_limit',
								'min' => 0,
								'max' => 55,
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'dependency' => array('element' => 'enable_description', 'value' => array('yes')),
								'group' => esc_html__('Content', 'dfd'),
							),
							array(
								'type' => 'dropdown',
								'heading' => __('Description Alignment', 'dfd'),
								'param_name' => 'info_alignment',
								'value' => array(
									__('Left', 'dfd') => 'text-left',
									__('Center', 'dfd') => 'text-center',
									__('Right', 'dfd') => 'text-right'
								),
								'group' => esc_html__('Content', 'dfd'),
							),
							array(
								'type' => 'dropdown',
								'heading' => __('Price position', 'dfd'),
								'param_name' => 'price_position',
								'value' => array(
									__('In heading', 'dfd') => 'top',
									__('In description', 'dfd') => 'bottom',
								),
								'dependency' => Array('element' => 'product_style', 'value' => array('style-1')),
								'group' => esc_html__('Content', 'dfd'),
							),
							array(
								'type' => 'dfd_param_heading',
								'text' => esc_html__('Title typography settings', 'dfd'),
								'param_name' => 'title_typography_heading',
								'edit_field_class' => 'ult-param-heading-wrapper no-top-margin vc_column vc_col-sm-12',
								'group' => esc_attr__('Typography', 'dfd'),
							),
							array(
								'type' => 'dfd_font_container_param',
								'heading' => '',
								'param_name' => 'title_font_options',
								'settings' => array(
									'fields' => array(
										'tag' => 'div',
										'letter_spacing',
										'font_size',
										'line_height',
										'color',
										'font_style'
									),
								),
								'dependency' => Array('element' => 'enable_title', 'value' => array('yes')),
								'group' => esc_attr__('Typography', 'dfd'),
							),
							array(
								'type' => 'dfd_param_heading',
								'text' => esc_html__('Subtitle typography settings', 'dfd'),
								'param_name' => 'subtitle_typography_heading',
								'edit_field_class' => 'ult-param-heading-wrapper vc_column vc_col-sm-12',
								'group' => esc_attr__('Typography', 'dfd'),
							),
							array(
								'type' => 'dfd_font_container_param',
								'heading' => '',
								'param_name' => 'subtitle_font_options',
								'settings' => array(
									'fields' => array(
										'tag' => 'div',
										'letter_spacing',
										'font_size',
										'line_height',
										'color',
										'font_style'
									),
								),
								'dependency' => Array('element' => 'enable_cat_tag', 'value' => array('yes')),
								'group' => esc_attr__('Typography', 'dfd'),
							),
							array(
								'type' => 'dfd_param_heading',
								'text' => esc_html__('Custom style settings', 'dfd'),
								'param_name' => 'style_heading',
								'edit_field_class' => 'ult-param-heading-wrapper no-top-margin vc_column vc_col-sm-12',
								'group' => esc_html__('Style Settings', 'dfd'),
							),
							array(
								'type' => 'dropdown',
								'heading' => __('Mask style', 'dfd'),
								'param_name' => 'mask_style',
								'value' => array(
									__('Theme default', 'dfd') => '',
									__('Simple color', 'dfd') => 'color',
									__('Gradient', 'dfd') => 'gradient',
								),
								'dependency' => Array('element' => 'product_style', 'value' => array('style-2')),
								'group' => esc_html__('Style Settings', 'dfd'),
							),
							array(
								'type' => 'colorpicker',
								'heading' => __('Mask color', 'dfd'),
								'param_name' => 'mask_color',
								'dependency' => Array('element' => 'mask_style', 'value' => array('color')),
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'group' => esc_html__('Style Settings', 'dfd'),
							),
							array(
								'type' => 'gradient',
								'param_name' => 'mask_gradient',
								'heading' => esc_html__('Mask gradient', 'dfd'),
								'dependency' => array('element' => 'mask_style', 'value' => array('gradient')),
								'group' => esc_html__('Style Settings', 'dfd'),
							),
							array(
								'type' => 'number',
								'heading' => esc_html__('Mask opacity', 'dfd'),
								'param_name' => 'mask_opacity',
								'value' => .8,
								'edit_field_class' => 'vc_column vc_col-sm-6 crum_vc',
								'dependency' => array('element' => 'mask_style', 'value' => array('gradient')),
								'group' => esc_html__('Style Settings', 'dfd'),
							),
							array(
								'type' => 'colorpicker',
								'heading' => __('Background', 'dfd'),
								'param_name' => 'background_color',
								'dependency' => Array('element' => 'product_style', 'value' => array('style-1')),
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'group' => esc_html__('Style Settings', 'dfd'),
							),
							array(
								'type' => 'colorpicker',
								'heading' => __('Product Description Text Color', 'dfd'),
								'param_name' => 'color_product_desc',
								'dependency' => Array('element' => 'enable_description', 'value' => array('yes')),
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'group' => esc_html__('Style Settings', 'dfd'),
							),
							array(
								'type' => 'number',
								'heading' => __('Price', 'dfd'),
								'param_name' => 'size_price',
								'min' => 10,
								'max' => 72,
								'suffix' => 'px',
								'group' => esc_html__('Style Settings', 'dfd'),
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'dependency' => Array('element' => 'enable_price', 'value' => array('yes')),
							),
							array(
								'type' => 'colorpicker',
								'heading' => __('Price Color', 'dfd'),
								'param_name' => 'color_price',
								'group' => esc_html__('Style Settings', 'dfd'),
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'dependency' => Array('element' => 'enable_price', 'value' => array('yes')),
							),
							array(
								'type' => 'colorpicker',
								'heading' => __('Star Ratings Color', 'dfd'),
								'param_name' => 'color_rating',
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'group' => esc_html__('Style Settings', 'dfd'),
								'dependency' => Array('element' => 'enable_rating', 'value' => array('yes')),
							),
							array(
								'type' => 'colorpicker',
								'heading' => __('Star Rating Background Color', 'dfd'),
								'param_name' => 'color_rating_bg',
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'group' => esc_html__('Style Settings', 'dfd'),
								'dependency' => Array('element' => 'enable_rating', 'value' => array('yes')),
							),
							array(
								'type' => 'colorpicker',
								'heading' => __('Sale Notification Text Color', 'dfd'),
								'param_name' => 'color_on_sale',
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'group' => esc_html__('Style Settings', 'dfd'),
							),
							array(
								'type' => 'colorpicker',
								'heading' => __('Sale Notification Background Color', 'dfd'),
								'param_name' => 'color_on_sale_bg',
								'edit_field_class' => 'vc_column vc_col-sm-6',
								'group' => esc_html__('Style Settings', 'dfd'),
							),
						)
					)
				);

				if(!isset($dfd_ronneby['dfd_woocommerce_templates_path']) || $dfd_ronneby['dfd_woocommerce_templates_path'] != '_old' && function_exists('vc_add_param')) {
					vc_add_param('woocomposer_product',array(
						'type' => 'dropdown',
						'heading' => __('Buttons color scheme', 'dfd'),
						'param_name' => 'buttons_color_scheme',
						'admin_label' => true,
						'value' => array(
								__('Dark','dfd') => 'dfd-buttons-dark',
								__('Light','dfd') => 'dfd-buttons-light',
							),
						'description' => __('Defines buttons color scheme if buttons are enabled', 'dfd'),
						'group' => 'Style Settings',
					));
				}
			}
		} 
		function WooComposer_Product($atts){
			extract($atts);
			
			$output = '';

			require_once(DFD_RONNEBY_PLUGIN_PATH .'inc/vc_custom/dfd_vc_addons/dfd_woo_shortcodes/design/design-single.php');
			
			$output .= Dfd_WooComposer_Single($atts);
			
			return $output;
		} 
	}
	
	new Dfd_Woo_Sinle_Product;
	
}