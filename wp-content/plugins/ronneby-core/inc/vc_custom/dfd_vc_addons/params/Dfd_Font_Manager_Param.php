<?php
if ( ! defined( 'ABSPATH' ) ) { exit; }
if(!class_exists('Dfd_Font_Manager_Param')) {
	class Dfd_Font_Manager_Param {
		function __construct() {	
			if(function_exists('vc_add_shortcode_param')) {
				vc_add_shortcode_param('dfd_google_fonts', array($this, 'dfd_google_fonts_settings'), DFD_RONNEBY_PLUGIN_URL .'inc/vc_custom/dfd_vc_addons/admin/vc_extend/js/dfd_additional_param.js');
				vc_add_shortcode_param('dfd_google_fonts_style', array($this, 'dfd_google_fonts_style_settings'));
			}
		}
	
		function dfd_google_fonts_settings($settings, $value)
		{
			//$dependency = vc_generate_dependencies_attributes($settings);
			$fonts = get_option('ultimate_selected_google_fonts');
			$html = '<div class="dfd_google_font_param_block">';
				$html .= '<input type="hidden" name="'.$settings['param_name'].'" class="wpb_vc_param_value vc-ultimate-google-font '.$settings['param_name'].' '.$settings['type'].'_field" value="'.$value.'" />';
				//$html .= '<form class="google-fonts-form">';
				$html .= '<select name="font_family" class="google-font-list">';
				$html .= '<option value="">'.__('Default','dfd').'</option>';
				if(!empty($fonts)) :
					foreach($fonts as $key => $font)
					{
						$selected = '';
						if($font['font_name'] == $value)
							$selected = 'selected';
						$html .= '<option value="'.$font['font_name'].'" '.$selected.'>'.esc_html($font['font_name']).'</option>';					
					}
				endif;
				$html .= '</select>';
				//$html .= '</form>';
			$html .= '</div>';
			return $html;
		}
		
		function dfd_google_fonts_style_settings($settings, $value)
		{
			//$dependency = vc_generate_dependencies_attributes($settings);
			$html = '<input type="hidden" name="'.$settings['param_name'].'" class="wpb_vc_param_value ugfont-style-value '.$settings['param_name'].' '.$settings['type'].'_field" value="'.$value.'" />';
			$html .= '<div class="ultimate_fstyle"></div>';
			return $html;
		}
		
	}
}

if(class_exists('Dfd_Font_Manager_Param')) {
	$Dfd_Font_Manager_Param = new Dfd_Font_Manager_Param();
}
