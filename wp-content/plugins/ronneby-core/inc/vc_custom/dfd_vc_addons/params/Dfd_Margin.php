<?php
if ( ! defined( 'ABSPATH' ) ) { exit; }
/*

# Usage - 
	array(
		"type" => "dfd_margins",
		"positions" => array(
			"Top" => "top",
			"Bottom" => "bottom",
			"Left" => "left",
			"Right" => "right"
		),
	),

*/
if(!class_exists('Dfd_Margin_Param'))
{
	class Dfd_Margin_Param
	{
		function __construct()
		{	
			if(function_exists('vc_add_shortcode_param'))
			{
				vc_add_shortcode_param('dfd_margins', array($this, 'dfd_margins_param'), DFD_RONNEBY_PLUGIN_URL .'inc/vc_custom/dfd_vc_addons/admin/vc_extend/js/dfd_additional_param.js');
			}
		}
	
		function dfd_margins_param($settings, $value)
		{
			//$dependency = vc_generate_dependencies_attributes($settings);
			$positions = $settings['positions'];
			$html = '<div class="ultimate-margins">
						<input type="hidden" name="'.$settings['param_name'].'" class="wpb_vc_param_value ultimate-margin-value '.$settings['param_name'].' '.$settings['type'].'_field" value="'.$value.'" />';
						foreach($positions as $key => $position) {
							$html .= '<div class="input-wrapper"><span>'.$key.'</span><div class="crum-number-field-wrap"><input type="text" data-hmargin="'.$position.'" class="ultimate-margin-inputs crum_number_field" id="margin-'.$key.'" /></div></div>';
						}
			$html .= '</div>';
			return $html;
		}
		
	}
}

if(class_exists('Dfd_Margin_Param'))
{
	$Dfd_Margin_Param = new Dfd_Margin_Param();
}
