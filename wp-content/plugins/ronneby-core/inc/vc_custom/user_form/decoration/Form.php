<?php

abstract class Dfd_FormDecorator {

	abstract public function generate($content);
}

class Main_Form_Decorator extends Dfd_FormDecorator {

	public function generate($content) {
		return $content;
	}

}

abstract class Dfd_Abstract_FormDecorator extends Dfd_FormDecorator {

	/**
	 *
	 * @var Dfd_FormDecorator $_simpleForm
	 */
	protected $_simpleForm;

	public function __construct(Dfd_FormDecorator $form) {
		$this->_simpleForm = $form;
	}

}

class Dfd_Simple_FormDecorator extends Dfd_Abstract_FormDecorator {

	function generate($content) {
		$settings = Dfd_contact_form_settings::instance()->getAllSettings();
		if (empty($settings))
			return false;
		$form_id = Dfd_contact_form_settings::instance()->getFormid();
		$general_class = '';
		$general_class .= $settings["preset"] . ' ' . $settings["el_class"];
		$general_class .= ' hover_style_input2_'.$settings["hover_style_input2"].' ';

		$animation_data = '';
		$el_class = '';
		if (isset($settings["module_animation"])) {
			if (!( $settings["module_animation"] == '' )) {
				$el_class = 'cr-animate-gen ';
				$animation_data = 'data-animate-type = "' . esc_attr($settings["module_animation"]) . '" ';
			}
		}
		?>
		<div class="wpb_wrapper <?php echo $el_class; ?>" <?php echo $animation_data; ?>>
			<div class="wpcf7">
				<div class="">
					<form action="" method="post" id="cf_<?php echo $form_id; ?>" class="wpcf7-form dfd_contact_form <?php echo $general_class; ?> cf_<?php echo $form_id; ?>" >
						<div class="container">
							<?php
							$class = "";
							if ($settings["preset"] == "preset3") {
								$class = "dfd-contact-form-style-1";
							};
							if ($settings["preset"] == "preset2") {
								$class = "dfd-contact-form-style-compact";
							};
							?>
							<div class="<?php echo $class; ?>">
								<?php echo $this->_simpleForm->generate($content); ?>
							</div>
						</div>
						<?php Dfd_reCaptchaDecorator::generate(); ?>
						<div class="wpcf7-response-output wpcf7-display-none wpcf7-validation-errors"></div>
					</form>
				</div>
			</div>
		</div>
		<?php
		unset($settings);
	}

}

class Dfd_ButtonDecorator extends Dfd_Abstract_FormDecorator {

	function generate($content) {
		echo $this->_simpleForm->generate($content);
		$settings = Dfd_contact_form_settings::instance()->getAllSettings();
		$message = isset($settings["btn_message"]) ? esc_attr($settings["btn_message"]) : "SEND MESSAGE";
		$width = isset($settings["btn_width"]) ? esc_attr($settings["btn_width"]) : "";
		$btn_align = "";
		if (isset($settings["btn_align"])) {
			$param_align = $settings["btn_align"];
			switch ($param_align) {
				case "left":
					$btn_align = "";
					break;
				case "center":
					$btn_align = "margin:0 auto; float: inherit;";
					if ($settings["preset"] == "preset1" || $settings["preset"] == "preset3") {
						$btn_align .= " text-align: center;";
					}
					break;
				case "right":
					$btn_align = "float:right;";
					break;
			}
		}
		?>
		<?php
		if(isset($settings['privacy_policy_link']) && !empty($settings['privacy_policy_link'])) {
			$privacy_policy_attr = vc_build_link($settings['privacy_policy_link']);
			if(isset($privacy_policy_attr['url']) && $privacy_policy_attr['url'] != '') {
				if(isset($privacy_policy_attr['title']) && $privacy_policy_attr['title'] != '') {
					$privacy_policy_title = $privacy_policy_attr['title'];
				} else {
					$privacy_policy_title = __('Policy', 'dfd');
				}
			?>
				<span class="wpcf7-form-control-wrap privacy-policy">
					<span class="label_text checkboxgroup">
						<span class="checkbox">
							<input name="policy_accept_required" type="hidden" value="1" />
							<input name="policy_accept" type="checkbox" value="1" />
							<span class="c_value policy-label">
								<?php echo !empty($settings['privacy_policy_prefix']) ? esc_html($settings['privacy_policy_prefix']) : ''; ?>
								<a
									<?php echo !empty($privacy_policy_attr['url']) ? 'href="'.esc_url($privacy_policy_attr['url']).'"' : ''; ?>
									<?php echo !empty($privacy_policy_attr['title']) ? 'title="'.esc_attr($privacy_policy_attr['title']).'"' : ''; ?>
									<?php echo !empty($privacy_policy_attr['target']) ? 'title="'.esc_attr(str_replace(' ', '', $privacy_policy_attr['target'])).'"' : ''; ?>
									<?php echo !empty($privacy_policy_attr['rel']) ? 'rel="'.esc_attr($privacy_policy_attr['rel']).'"' : ''; ?>
								class="dfd-custom-link">
									<?php echo esc_html($privacy_policy_title); ?>
								</a>
							</span>
						</span>
					</span>
				</span>
			<?php
			}
		}
		?>
		<p class="<?php echo $width; ?> form_button" style="<?php echo $btn_align ?>">
			<?php if(isset($settings['use_button_border_decor']) && $settings['use_button_border_decor'] == 'yes') { ?>
				<span class="dfd-cf-border-decoration"></span>
			<?php } ?>
			<input type="submit" value="<?php echo $message; ?>" class="wpcf7-submit">
		</p>
		<div class="clear"></div>
		<?php
		unset($settings);
	}

}

class Dfd_reCaptchaDecorator {

	static function generate($content = "") {
//        echo $this->_simpleForm->generate($content);
//        $settings = Dfd_contact_form_settings::instance()->getAllSettings();
		$recaptcha = new Dfd_Contact_Form_Recaptcha();
		?>
		<?php echo $recaptcha->toHtml(); ?>
		<?php
	}

}

class Dfd_MessageDecorator extends Dfd_Abstract_FormDecorator {

	function generate($content) {
		echo $this->_simpleForm->generate($content);
		?>  

		<?php
	}

}
