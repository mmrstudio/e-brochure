<?php
if(!defined('ABSPATH')) {
	exit;
}

if(!class_exists('Dfd_Taxonomies_Custom_Fields')) {
	class Dfd_Taxonomies_Custom_Fields {
		
		var $taxonomies = array('','my-product_','gallery_');
		
		function __construct() {
			add_action('admin_enqueue_scripts',array($this,'dfd_post_cat_reqister_scripts'));
			foreach($this->taxonomies as $tax) {
				add_action($tax.'category_add_form_fields',array($this,'dfd_taxonomy_add_new_meta_field'));
				add_action($tax.'category_edit_form_fields',array($this,'dfd_taxonomy_edit_meta_field'));
				add_action('edited_'.$tax.'category',array($this,'save_taxonomy_custom_meta'));
				add_action('create_'.$tax.'category',array($this,'save_taxonomy_custom_meta'));
			}
		}
		
		function dfd_taxonomy_add_new_meta_field() {
			?>
			<div class="form-field">
				<label for="term_meta[custom_term_meta]"><?php _e( 'Category&#0146;s icon', 'dfd' ); ?></label>
				<input type="text" name="term_meta[custom_term_meta]" id="term_meta[custom_term_meta]" class="iconname" value="" style="width:50%;" />
				<a href="#" class="updateButton crum-icon-add"><?php _e('Add Icon', 'dfd'); ?></a>
			</div>
			<div class="form-field">
				<label for="term_meta[custom_term_meta_color]"><?php _e( 'Category&#0146;s color', 'dfd' ); ?></label>
				<input type="text" id="dfd-category-colorpicker" name="term_meta[custom_term_meta_color]" value="" />
				<script type="text/javascript">
					jQuery(document).ready(function($) {
						$("#dfd-category-colorpicker").wpColorPicker();
					});
				</script>
			</div>
		<?php
		}

		function dfd_taxonomy_edit_meta_field($term) {
			$t_id = $term->term_id;

			$term_meta = get_option( "taxonomy_$t_id" ); ?>
			<tr class="form-field">
				<th scope="row" valign="top"><label for="term_meta[custom_term_meta]"><?php _e( 'Category&#0146;s icon', 'dfd' ); ?></label></th>
				<td>
					<input type="text" name="term_meta[custom_term_meta]" id="term_meta[custom_term_meta]" class="iconname" value="<?php echo esc_attr( $term_meta['custom_term_meta'] ) ? esc_attr( $term_meta['custom_term_meta'] ) : ''; ?>" style="width:50%;" />
					<a href="#" class="updateButton crum-icon-add"><?php _e('Add Icon', 'dfd'); ?></a>
				</td>
			</tr>
			<tr class="form-field">
				<th scope="row" valign="top"><label for="term_meta[custom_term_meta_color]"><?php _e( 'Category&#0146;s color', 'dfd' ); ?></label></th>
				<td>
					<input type="text" id="dfd-category-colorpicker" name="term_meta[custom_term_meta_color]" value="<?php echo esc_attr( $term_meta['custom_term_meta_color'] ) ? esc_attr( $term_meta['custom_term_meta_color'] ) : ''; ?>" />
					<script type="text/javascript">
						jQuery(document).ready(function($) {
							$("#dfd-category-colorpicker").wpColorPicker();
						});
					</script>
				</td>
			</tr>
		<?php
		}
		
		function save_taxonomy_custom_meta( $term_id ) {
			if ( isset( $_POST['term_meta'] ) ) {
				$t_id = $term_id;
				$term_meta = get_option( "taxonomy_$t_id" );
				$cat_keys = array_keys( $_POST['term_meta'] );
				foreach ( $cat_keys as $key ) {
					if ( isset ( $_POST['term_meta'][$key] ) ) {
						$term_meta[$key] = $_POST['term_meta'][$key];
					}
				}
				update_option( "taxonomy_$t_id", $term_meta );
			}
		}
		
		function dfd_post_cat_reqister_scripts() {
			wp_enqueue_script('wp-color-picker');
			wp_enqueue_style('wp-color-picker');
		}
	}
	
	$Dfd_Taxonomies_Custom_Fields = new Dfd_Taxonomies_Custom_Fields();
}