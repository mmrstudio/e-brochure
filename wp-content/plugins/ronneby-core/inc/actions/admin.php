<?php
if(!defined('ABSPATH')) {
	exit;
}

if(!class_exists('Dfd_Ronneby_Admin_Actions')) {
	class Dfd_Ronneby_Admin_Actions {
		public $pagenow;
		
		public function __construct() {
			global $pagenow;
			$this->pagenow = $pagenow;
			
			$this->init();
		}
		public function init() {
			$this->actions();
		}
		public function actions() {
			add_action('after_setup_theme', array($this, 'enqueueExtras'));
			add_action('switch_theme', array($this, 'generateDynamicStyles'));
			add_action('redux/options/ronneby/saved', array($this, 'generateDynamicStyles'));
			add_action('wbc_importer_after_theme_options_import', array($this, 'generateDynamicStyles'));
		}
		public function enqueueExtras() {
			global $dfd_ronneby;
			if($this->pagenow == "post.php" || $this->pagenow == "post-new.php" || $this->pagenow == "edit.php" || $this->pagenow == 'admin-ajax.php') {
				# Styled button shortcode
				if(!isset($dfd_ronneby['enable_styled_button']) || $dfd_ronneby['enable_styled_button'] == 'on') {
					require_once DFD_RONNEBY_PLUGIN_PATH.'/inc/styled-button/init.php';
				}
			}
		}
		/*
		 * Dynamic styles generator. Writes styles to uploads/redux/options.css file
		 */
		public function generateDynamicStyles() {
			global $dfd_ronneby;

			if(isset($dfd_ronneby['enqueue_styles_file']) && $dfd_ronneby['enqueue_styles_file'] == 'on') {
				/** Save on different directory if on multisite **/
				$aq_uploads_dir = ReduxFramework::$_upload_dir;

				/** Capture CSS output **/
				if(file_exists(get_template_directory().'/inc/lib/dynamic_styles_config.php')) {
					ob_start();
					require get_template_directory().'/inc/lib/dynamic_styles_config.php';
					$css = ob_get_clean();

					/** Write to options.css file **/
					global $wp_filesystem;
					if (empty($wp_filesystem)) {
						require_once (ABSPATH . '/wp-admin/includes/file.php');
						WP_Filesystem();
					}
					if( !empty($wp_filesystem) && !(is_wp_error($wp_filesystem->errors) && $wp_filesystem->errors->get_error_code()) ) {
						$wp_filesystem->put_contents(
							$aq_uploads_dir . 'options.css',
							$css,
							FS_CHMOD_FILE
						);
					} else {
						file_put_contents($aq_uploads_dir . 'options.css', $css);
					}
				}
			}
		}
	}
	
	$Dfd_Ronneby_Admin_Actions = new Dfd_Ronneby_Admin_Actions();
}
