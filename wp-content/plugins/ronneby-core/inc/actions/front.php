<?php
if(!defined('ABSPATH')) {
	exit;
}

if(!class_exists('Dfd_Front_Core_Actions')) {
	class Dfd_Front_Core_Actions {
		public $pagenow;
		
		public function __construct() {
			global $pagenow;
			$this->pagenow = $pagenow;
			
			$this->init();
		}
		public function init() {
			$this->actions();
			$this->filters();
		}
		public function actions() {
			add_action('after_setup_theme', array($this, 'setupThemeAction'));
			add_action('init', array($this, 'startSession'));
			add_action('wp_head', array($this, 'headFacebookMetatags'));
			add_action('wp_head', array($this, 'feedlink'), -2);
			add_action('init', array($this, 'removeQueryStrings'));
			if(isset($this->pagenow) && $this->pagenow == 'wp-login.php') {
				add_action('login_head', array($this, 'customLoginLogo'));
			}
		}
		public function setupThemeAction() {
			global $dfd_ronneby;
			if(!isset($dfd_ronneby['enable_styled_button']) || $dfd_ronneby['enable_styled_button'] == 'on') {
				require_once DFD_RONNEBY_PLUGIN_PATH.'/inc/styled-button/init.php';
			}
		}
		public function filters() {
			add_filter('widget_text', 'do_shortcode');
		}
		public function startSession() {
			if(session_id() == '' || (function_exists('session_status') && session_status() == PHP_SESSION_NONE)) {
				@session_start();
			}
		}
		/*
		 * Add og: meta tags for facebook share
		 */
		public function headFacebookMetatags() {
			if(has_post_thumbnail()) {
				$thumb_id = get_post_thumbnail_id();
				$img_src = wp_get_attachment_image_src($thumb_id, 'full');
				if(isset($img_src[0]) && !empty($img_src[0])) {
					echo '<meta property="og:image" content="'.esc_url($img_src[0]).'" />';
				}
				if(isset($img_src[1]) && !empty($img_src[1])) {
					echo '<meta property="og:image:width" content="'.esc_attr($img_src[1]).'" />';
				}
				if(isset($img_src[2]) && !empty($img_src[2])) {
					echo '<meta property="og:image:height" content="'.esc_attr($img_src[2]).'" />';
				}
				echo '<meta property="og:url" content="'.esc_attr(get_permalink()).'" />';
				echo '<meta property="og:title" content="'.esc_attr(get_the_title()).'" />';
			}
		}
		public function feedlink() {
			$count = wp_count_posts('post');
			if ($count->publish > 0) {
				echo "\n\t<link rel=\"alternate\" type=\"application/rss+xml\" title=\"". get_bloginfo('name') ." Feed\" href=\"". home_url() ."/feed/\">\n";
			}
		}
		public function customLoginLogo() {
			global $dfd_ronneby;
	
			$before_login_page_css = $login_page_css = $login_page_js = '';

			$title_color = '#242424';
			$text_color = '#565656';
			$form_bg = '#ffffff';
			$input_bg = 'transparent';
			$input_border = '#dddddd';

			if(isset($dfd_ronneby['custom_logo_image']['url']) && $dfd_ronneby['custom_logo_image']['url']){
				$custom_logo = $dfd_ronneby['custom_logo_image']['url'];
			} else {
				$custom_logo = get_template_directory_uri() .'/assets/img/logo.png';
			}

			$logo_width = (isset($dfd_ronneby['header_logo_width']) && !empty($dfd_ronneby['header_logo_width'])) ? $dfd_ronneby['header_logo_width'] : 206;

			$logo_height = (isset($dfd_ronneby['header_logo_height']) && !empty($dfd_ronneby['header_logo_height'])) ? $dfd_ronneby['header_logo_height'] : 42;

			$login_page_css .= 'body.login{background:#fff;}
					body.login #login {position: relative;top: 50%;margin: 0 auto;padding: 0;-webkit-transform: translateY(-50%);-moz-transform: translateY(-50%);-o-transform: translateY(-50%);transform: translateY(-50%);}';			

			if(isset($dfd_ronneby['custom_login_page']) && $dfd_ronneby['custom_login_page'] == 'on') {
				$before_login_page_css = '<style type="text/css">@font-face {font-family: "Montserrat";font-style: normal;font-weight: 400;src: local("Montserrat-Regular"), url(http://fonts.gstatic.com/s/montserrat/v6/zhcz-_WihjSQC0oHJ9TCYPk_vArhqVIZ0nv9q090hN8.woff2) format("woff2"); unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000; }@font-face {font-family: "Montserrat";font-style: normal;font-weight: 700;src: local("Montserrat-Bold"), url(http://fonts.gstatic.com/s/montserrat/v6/IQHow_FEYlDC4Gzy_m8fcoWiMMZ7xLd792ULpGE4W_Y.woff2) format("woff2");unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;}@font-face {font-family: "Lora";font-style: italic;font-weight: 400;src: local("Lora Italic"), local("Lora-Italic"), url(http://fonts.gstatic.com/s/lora/v9/_RSiB1sBuflZfa9fxV8cOg.woff2) format("woff2");unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;}@font-face {font-family: "Lora";font-style: normal;font-weight: 400;src: local("Lora"), local("Lora-Regular"), url(http://fonts.gstatic.com/s/lora/v9/4vqKRIwnQQGUQQh-PnvdMA.woff2) format("woff2");unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;}@font-face {font-family: "Raleway";font-style: normal;font-weight: 400;src: local("Raleway"), local("Raleway-Regular"), url(http://fonts.gstatic.com/s/raleway/v10/yQiAaD56cjx1AooMTSghGfY6323mHUZFJMgTvxaG2iE.woff2) format("woff2");unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;}@font-face {font-family: "Raleway";font-style: normal;font-weight: 400;src: local("Raleway"), local("Raleway-Regular"), url(http://fonts.gstatic.com/s/raleway/v10/0dTEPzkLWceF7z0koJaX1A.woff2) format("woff2");unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;}</style>';
				if(isset($dfd_ronneby['login_page_color_scheme']) && $dfd_ronneby['login_page_color_scheme'] == 'dark') {
					$title_color = '#ffffff';
					$text_color = 'rgba(255,255,255,.2)';
					$form_bg = '#2d2d2d';
					$input_bg = 'rgba(255,255,255,.05)';
					$input_border = 'rgba(255,255,255,.1)';
					if(isset($dfd_ronneby['custom_logo_image_second']['url']) && !empty($dfd_ronneby['custom_logo_image_second']['url']))
						$custom_logo = $dfd_ronneby['custom_logo_image_second']['url'];
				}
				if(isset($dfd_ronneby['custom_login_page_logo']) && $dfd_ronneby['custom_login_page_logo'] == 'off') {
					$login_page_css .= '.login h1 a, body.login > a.logo {display: none !important;}';
				}
				if(isset($dfd_ronneby['login_page_bg_color']) && $dfd_ronneby['login_page_bg_color'] != '') {
					$login_page_css .= 'body.login{background-color:'.esc_attr($dfd_ronneby['login_page_bg_color']).';}';
				}
				if(isset($dfd_ronneby['login_page_bg_image']['url']) && $dfd_ronneby['login_page_bg_image']['url'] != '') {
					$background_size = (isset($dfd_ronneby['login_page_bg_image_size']) && $dfd_ronneby['login_page_bg_image_size'] != '') ? $dfd_ronneby['login_page_bg_image_size'] : 'initial';
					$login_page_css .= 'body.login{background-image:url('.esc_attr($dfd_ronneby['login_page_bg_image']['url']).');background-size: '.esc_attr($background_size).';background-position: center center;background-repeat: no-repeat;}';
				}
				$login_page_css .= '#login p.login-title {position: absolute;left: 0;bottom: 100%;margin-bottom: 10px;font-family: "Montserrat";font-weight: 700;font-size: 31px;letter-spacing:-2px;line-height: 1;color: '.esc_attr($title_color).';}';
				$login_page_css .= '#login p.login-title span {font-family: "Lora";font-weight: 400;font-style: italic;letter-spacing:0;color: inherit;}';
				$login_page_css .= 'body > .logo {display: block;text-indent: 9999em;position: relative;top: 50px;overflow: hidden; margin: 0 auto;color:transparent;}';
				$login_page_css .= 'body.login form {margin: 0;padding: 24px; background: '.esc_attr($form_bg).';border-radius: 2px;}';
				$login_page_css .= 'body.login form label {font-family: "Lora";font-size: 14px;font-style: normal;color: '.esc_attr($text_color).';}';
				$login_page_css .= 'body.login form .forgetmenot label {font-size: 14px;;}';
				$login_page_css .= 'body.login form input, body.login form input[type="text"], body.login form input[type="email"], body.login form input[type="password"] {padding: 8px;background: '.esc_attr($input_bg).';color: '.esc_attr($title_color).';border-color: '.esc_attr($input_border).';border-radius: 2px;}';
				$login_page_css .= 'body.login #nav {color: '.esc_attr($title_color).';}';
				$login_page_css .= 'body.login #nav a {font-family: "Lora";font-size: 14px;font-style: normal;color: inherit;border-bottom: 1px dotted #c39f77;-webkit-transition: border .3s ease 0s;-moz-transition: border .3s ease 0s;-o-transition: border .3s ease 0s;transition: border .3s ease 0s;}';
				$login_page_css .= 'body.login #nav a:hover {color: inherit;border-bottom-style: solid;}';
				$login_page_css .= 'body.login #backtoblog {color: '.esc_attr($title_color).';}';
				$login_page_css .= 'body.login #backtoblog a {font-family: "Lora";font-size: 14px;font-style: normal;color: inherit;}';
				$login_page_css .= 'body.login #backtoblog a:hover {color: inherit;}';
				$login_page_css .= 'body.login form p.submit input[type="submit"] {font-family: "Lora";font-size: 14px;height: auto;line-height:48px;padding: 0 35px;color: '.esc_attr($title_color).';background: #c39f77;border: none;border-radius: 2px;box-shadow: none;text-shadow: none;}';
				$login_page_css .= 'body.login form .forgetmenot label {line-height: 48px;}';
				$login_page_css .= 'body.login form .forgetmenot label input[type="checkbox"] {position: relative;width: 20px; height: 20px;background: '.esc_attr($input_bg).';border-color: '.esc_attr($input_border).';border-radius: 0;}';
				$login_page_css .= 'body.login form .forgetmenot label input[type="checkbox"]:before {content: "";width: 12px;height: 12px;position: absolute; top: 50%;left: 50%;margin-top: -6px;margin-left: -6px;background: '.esc_attr($input_border).';-webkit-transform: scale(0);-moz-transform: scale(0);-o-transform: scale(0);transform: scale(0);-webkit-transition: all .3s ease 0s;-moz-transition: all .3s ease 0s;-o-transition: all .3s ease 0s;transition: all .3s ease 0s;}';
				$login_page_css .= 'body.login form .forgetmenot label input[type="checkbox"]:checked:before {-webkit-transform: scale(1);-moz-transform: scale(1);-o-transform: scale(1);transform: scale(1);}';
				$login_page_js .=	'<script type="text/javascript">
										(function($) {
											$(document).ready(function() {
												$("#loginform").prepend("<p class=\"login-title\">'.esc_html__('Log in on', 'dfd').' <span>'.esc_html__('site', 'dfd').'</span></p>");
												if($("#login > h1 > a")) {
													var $logo = $("#login > h1 > a"),
														$logoClone = $logo.clone();
													$logoClone.prependTo("body").addClass("logo");
													$logo.remove();
												}
											});
										})(jQuery);
									</script>';
			}

			$login_page_css .= '.logo, .login h1 a { background-repeat: no-repeat; background-image:url('. esc_url($custom_logo) .') !important; height: auto !important; min-height: '.esc_attr($logo_height).'px !important; width: '.esc_attr($logo_width).'px !important; background-size: contain !important;}';

			$login_page_css = '<style type="text/css">'.$login_page_css.'</style>';

			echo $before_login_page_css;
			echo $login_page_css;
			echo $login_page_js;
		}
		public function removeQueryStrings() {
			global $dfd_ronneby;
			
			if(isset($dfd_ronneby['remove_query_strings']) && $dfd_ronneby['remove_query_strings'] === 'on') {
				add_filter('script_loader_src', array($this, 'removeQueryStringFilter'), 9999);
				add_filter('style_loader_src', array($this, 'removeQueryStringFilter'), 9999);
			}
		}
		public function removeQueryStringFilter($src) {
			$src = remove_query_arg('ver', $src);

			return $src;
		}
	}
	
	$Dfd_Front_Core_Actions = new Dfd_Front_Core_Actions();
}