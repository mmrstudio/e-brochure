<?php
if(!defined('ABSPATH')) {
	exit;
}

if(!class_exists('Dfd_Mixed_Core_Actions')) {
	class Dfd_Mixed_Core_Actions {
		public $pagenow;
		
		public function __construct() {
			global $pagenow;
			$this->pagenow = $pagenow;
			
			$this->init();
		}
		public function init() {
			$this->actions();
		}
		public function actions() {
			add_action('vc_build_admin_page', array($this, 'hideVcElements'));
			add_action('vc_load_shortcode', array($this, 'hideVcElements'));
		}
		public function hideVcElements() {
			global $dfd_ronneby;

			if(function_exists( 'vc_remove_element' ) && !isset($dfd_ronneby['enable_default_modules']) || $dfd_ronneby['enable_default_modules'] != '1') {
				vc_remove_element('vc_wp_search');
				vc_remove_element('vc_wp_meta');
				vc_remove_element('vc_wp_recentcomments');
				vc_remove_element('vc_wp_calendar');
				vc_remove_element('vc_wp_pages');
				vc_remove_element('vc_wp_tagcloud');
				vc_remove_element('vc_wp_custommenu');
				vc_remove_element('vc_wp_text');
				vc_remove_element('vc_wp_posts');
				vc_remove_element('vc_wp_links');
				vc_remove_element('vc_wp_categories');
				vc_remove_element('vc_wp_archives');
				vc_remove_element('vc_wp_rss');
				vc_remove_element('vc_gallery');
				vc_remove_element('vc_teaser_grid');
				vc_remove_element('vc_button');
				vc_remove_element('vc_cta_button');
				vc_remove_element('vc_posts_grid');
				vc_remove_element('vc_images_carousel');
				vc_remove_element('vc_separator');
				vc_remove_element('vc_text_separator');
				vc_remove_element('vc_message');
				vc_remove_element('vc_facebook');
				vc_remove_element('vc_tweetmeme');
				vc_remove_element('vc_googleplus');
				vc_remove_element('vc_pinterest');
				vc_remove_element('vc_toggle');
				vc_remove_element('vc_posts_slider');
				vc_remove_element('vc_button2');
				vc_remove_element('vc_cta_button2');
				vc_remove_element('vc_gmaps');
				vc_remove_element('vc_flickr');
				vc_remove_element('vc_progress_bar');
				vc_remove_element('vc_pie');
				vc_remove_element('vc_empty_space');
				vc_remove_element('vc_custom_heading');
				vc_remove_element('vc_basic_grid');
				vc_remove_element('vc_media_grid');
				vc_remove_element('vc_masonry_grid');
				vc_remove_element('vc_masonry_media_grid');
				vc_remove_element('vc_icon');
				vc_remove_element('vc_btn');
				vc_remove_element('vc_cta');
				vc_remove_element('vc_line_chart');
				vc_remove_element('vc_round_chart');
	//			vc_remove_element('vc_single_image');
				vc_remove_element('vc_video');
				vc_remove_element('vc_tta_tabs');
				vc_remove_element('vc_tta_tour');
				vc_remove_element('vc_tta_accordion');
				if ( class_exists('WooCommerce') ) {
					vc_remove_element( 'woocommerce_cart' );
					vc_remove_element( 'woocommerce_checkout' );
					vc_remove_element( 'woocommerce_order_tracking' );
					vc_remove_element( 'woocommerce_my_account' );
					vc_remove_element( 'product' );
					vc_remove_element( 'products' );
					vc_remove_element( 'add_to_cart' );
					vc_remove_element( 'add_to_cart_url' );
					vc_remove_element( 'product_page' );
					vc_remove_element( 'product_categories' );
					vc_remove_element( 'product_attribute' );
					vc_remove_element( 'product_category' );
					vc_remove_element( 'recent_products' );
					vc_remove_element( 'featured_products' );
					vc_remove_element( 'sale_products' );
					vc_remove_element( 'best_selling_products' );
					vc_remove_element( 'top_rated_products' );
				}
			}
		}
	}
	
	$Dfd_Mixed_Core_Actions = new Dfd_Mixed_Core_Actions();
}