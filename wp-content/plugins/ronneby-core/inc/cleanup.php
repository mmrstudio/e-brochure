<?php
if ( ! defined( 'ABSPATH' ) ) { exit; }

if(!class_exists('Dfd_Ronneby_Cleanup')) {
	class Dfd_Ronneby_Cleanup {
		function __construct() {
			add_action('after_setup_theme', array($this, 'init'));
			add_action('get_header', array($this, 'removeAdminBarOffset'));
		}
		function init() {
			if($this->enableRootRelativeUrls()) {
				$this->rootRelativeUrlsAction();
			}
			
			add_filter('style_loader_tag', array($this, 'cleanStyleTag'));
			add_filter('body_class', array($this, 'bodyClass'));
			add_filter('wp_get_attachment_link', array($this, 'attachmentLinkClass'), 10, 1);
			add_filter('img_caption_shortcode', array($this, 'caption'), 10, 3);
			add_filter('excerpt_more', array($this, 'clearExcerptMore'));
			add_filter('get_search_query', array($this, 'searchQuery'));
			add_filter('request', array($this, 'requestFilter'));
		}
		function rootRelativeUrlsAction() {
			$filters = array(
				'bloginfo_url',
				'theme_root_uri',
				'stylesheet_directory_uri',
				'template_directory_uri',
				'plugins_url',
				'the_permalink',
				'wp_list_pages',
				'wp_list_categories',
				'wp_nav_menu',
				'the_content_more_link',
				'the_tags',
				'get_pagenum_link',
				'get_comment_link',
				'month_link',
				'day_link',
				'year_link',
				'tag_link',
				'the_author_posts_link'
			);

			foreach($filters as $filter) {
				$this->addFilter($filter, 'rootRelativeUrl');
			}
			
			add_filter('script_loader_src', array($this, 'fixDuplicateSubfolderUrls'));
			add_filter('style_loader_src', array($this, 'fixDuplicateSubfolderUrls'));
		}
		function enableRootRelativeUrls() {
			return !(is_admin() && in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'))) && current_theme_supports('root-relative-urls');
		}
		function addFilter($tag = '', $callback = '') {
			add_filter($tag, array($this, $callback));
		}
		/**
		 * Relative URLs
		 *
		 * WordPress likes to use absolute URLs on everything - let's clean that up.
		 * Inspired by http://www.456bereastreet.com/archive/201010/how_to_make_wordpress_urls_root_relative/
		 *
		 * You can enable/disable this feature in config.php:
		 * current_theme_supports('root-relative-urls');
		 *
		 * @author Scott Walkinshaw <scott.walkinshaw@gmail.com>
		 */
		function rootRelativeUrl($input) {
			$output = preg_replace_callback(
				'!(https?://[^/|"]+)([^"]+)?!',
				create_function(
					'$matches',
					// If full URL is home_url("/") and this isn't a subdir install, return a slash for relative root
					'if (isset($matches[0]) && $matches[0] === home_url("/") && str_replace("http://", "", home_url("/", "http"))==$_SERVER["HTTP_HOST"]) { return "/";' .
						// If domain is equal to home_url("/"), then make URL relative
						'} elseif (isset($matches[0]) && strpos($matches[0], home_url("/")) !== false) { return $matches[2];' .
						// If domain is not equal to home_url("/"), do not make external link relative
						'} else { return $matches[0]; };'
				),
				$input
			);

			return $output;
		}
		/**
		 * Terrible workaround to remove the duplicate subfolder in the src of <script> and <link> tags
		 * Example: /subfolder/subfolder/css/style.css
		 */
		function fixDuplicateSubfolderUrls($input) {
			$output = $this->rootRelativeUrl($input);
			preg_match_all('!([^/]+)/([^/]+)!', $output, $matches);

			if (isset($matches[1][0]) && isset($matches[2][0])) {
				if ($matches[1][0] === $matches[2][0]) {
					$output = substr($output, strlen($matches[1][0]) + 1);
				}
			}

			return $output;
		}
		/**
		 * Clean up output of stylesheet <link> tags
		 */
		function cleanStyleTag($input) {
			preg_match_all("!<link rel='stylesheet'\s?(id='[^']+')?\s+href='(.*)' type='text/css' media='(.*)' />!", $input, $matches);
			// Only display media if it's print
			$media = $matches[3][0] === 'print' ? ' media="print"' : '';
			return '<link rel="stylesheet" href="' . $matches[2][0] . '"' . $media . '>' . "\n";
		}
		/**
		 * Add and remove body_class() classes
		 */
		function bodyClass($classes) {
			// Add post/page slug
			if (is_single() || is_page() && !is_front_page()) {
				$classes[] = 'dfd-page-title-'.basename(get_permalink());
			}
			
			if(is_rtl()) {
				$classes[] = 'dfd-rtl';
			}
			
			if ((!isset($dfd_ronneby['scroll_animation']) || strcmp($dfd_ronneby['scroll_animation'],'off') !== 0)) {
				$classes[] = 'dfd-smooth-scroll';
			}
			
			if(isset($dfd_ronneby['enable_deep_links']) && $dfd_ronneby['enable_deep_links'] == 'on') {
				$classes[] = 'dfd-pp-deeplinks';
			}

			// Remove unnecessary classes
			$home_id_class = 'page-id-' . get_option('page_on_front');
			$remove_classes = array(
				'page-template-default',
				$home_id_class
			);
			$classes = array_diff($classes, $remove_classes);

			return $classes;
		}
		/**
		 * Add class="thumbnail" to attachment items
		 */
		function attachmentLinkClass($html) {
			$postid = get_the_ID();
			$html = str_replace('<a', '<a class="thumbnail"', $html);
			return $html;
		}
		/**
		 * Add Bootstrap thumbnail styling to images with captions
		 * Use <figure> and <figcaption>
		 *
		 * @link http://justintadlock.com/archives/2011/07/01/captions-in-wordpress
		 */
		function caption($output, $attr, $content) {
			if (is_feed()) {
				return $output;
			}

			$defaults = array(
				'id' => '',
				'align' => 'alignnone',
				'width' => '',
				'caption' => ''
			);

			$attr = shortcode_atts($defaults, $attr);

			// If the width is less than 1 or there is no caption, return the content wrapped between the [caption] tags
			if ($attr['width'] < 1 || empty($attr['caption'])) {
				return $content;
			}

			// Set up the attributes for the caption <figure>
			$attributes = (!empty($attr['id']) ? ' id="' . esc_attr($attr['id']) . '"' : '');
			$attributes .= ' class="thumbnail wp-caption ' . esc_attr($attr['align']) . '"';
			$attributes .= ' style="width: ' . esc_attr($attr['width']) . 'px"';

			$output = '<figure' . $attributes . '>';
			$output .= do_shortcode($content);
			$output .= '<figcaption class="caption wp-caption-text">' . $attr['caption'] . '</figcaption>';
			$output .= '</figure>';

			return $output;
		}
		/**
		 * Clean up the_excerpt()
		 */
		function clearExcerptMore($more) {
			return '';
		}
		/**
		 * Fix for get_search_query() returning +'s between search terms
		 */
		function searchQuery($escaped = true) {
			$query = apply_filters('crum_search_query', get_query_var('s'));

			if ($escaped) {
				$query = esc_attr($query);
			}

			return urldecode($query);
		}
		/**
		 * Fix for empty search queries redirecting to home page
		 *
		 * @link http://wordpress.org/support/topic/blank-search-sends-you-to-the-homepage#post-1772565
		 * @link http://core.trac.wordpress.org/ticket/11330
		 */
		function requestFilter($query_vars) {
			if (isset($_GET['s']) && empty($_GET['s'])) {
				$query_vars['s'] = '';
			}

			return $query_vars;
		}
		/*
		 * Remove the admin bar offset
		 */
		function removeAdminBarOffset() {
			if(is_admin_bar_showing()) {
				remove_action( 'wp_head', '_admin_bar_bump_cb' );
			}
		}
	}
	
	$Dfd_Ronneby_Cleanup = new Dfd_Ronneby_Cleanup();
}