<?php
if(!defined('ABSPATH')) {
	exit;
}

if(!class_exists('Dfd_Ronneby_Core_Includes')) {
	class Dfd_Ronneby_Core_Includes {
		public $pagenow;
		function __construct() {
			global $pagenow;
			
			$this->pagenow = $pagenow;
			$this->mixedInclude();
			if(is_admin()) {
				$this->adminInclude();
			} else {
				$this->frontInclude();
			}
		}
		function adminInclude() {
			if(!is_customize_preview()) {
				if(isset($this->pagenow) && in_array($this->pagenow, array('edit-tags.php', 'term.php'))) {
					require_once DFD_RONNEBY_PLUGIN_PATH .'inc/post-types/meta-fields.php';
				}
			}
			if(isset($this->pagenow) && (in_array($this->pagenow, array('post.php', 'post-new.php', 'page.php', 'page-new.php', 'admin-ajax.php')) || ($this->pagenow == 'admin.php') && isset($_GET['page']) && $_GET['page'] == 'vc-roles')) {
				require_once DFD_RONNEBY_PLUGIN_PATH .'inc/shortcodes/tinymce-shortcodes.php';
			
				$this->addVcExtensions();
			}
		}
		function frontInclude() {
			require_once DFD_RONNEBY_PLUGIN_PATH.'/inc/cleanup.php';
			
			require_once DFD_RONNEBY_PLUGIN_PATH .'inc/shortcodes/tooltip-shortcode.php';
			
			require_once DFD_RONNEBY_PLUGIN_PATH .'inc/social/twitteroauth.php';
			
			$this->addVcExtensions();
		}
		function mixedInclude() {
			require_once DFD_RONNEBY_PLUGIN_PATH .'inc/redux_extensions/extensions_loader.php';

			require_once DFD_RONNEBY_PLUGIN_PATH .'inc/redux_framework/ReduxCore/framework.php';
			
			require_once DFD_RONNEBY_PLUGIN_PATH .'inc/post-types/post-type.php';
			
			require_once DFD_RONNEBY_PLUGIN_PATH .'inc/actions/init.php';
			
			if(class_exists('WooCommerce')) {
				require_once get_template_directory().'/inc/lib/woocommerce/woocommerce.php';
			}
			
			if(defined('YITH_WCWL') && YITH_WCWL) {
				require_once get_template_directory().'/inc/lib/woocommerce/wishlist.php';
			}
		}
		public function addVcExtensions() {
			if(class_exists('Vc_Manager', false)) {
				require_once DFD_RONNEBY_PLUGIN_PATH .'inc/vc_custom/dfd_vc_addons/Dfd_VC_Addons.php';

				require_once DFD_RONNEBY_PLUGIN_PATH .'inc/vc_custom/dfd_vc_addons.php';
			}
		}
	}
	
	$Dfd_Ronneby_Core_Includes = new Dfd_Ronneby_Core_Includes();
}