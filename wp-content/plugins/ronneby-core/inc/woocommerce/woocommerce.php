<?php
if ( ! defined( 'ABSPATH' ) ) { exit; }
/**
 * Woocommerce support
 */

if(!class_exists('Dfd_WooCommerce_Ronneby_Init')) {
	class Dfd_WooCommerce_Ronneby_Init {
		public $pagenow;
		function __construct() {
			if(!is_admin()) {
				$this->frontActions();
			}
		}
		function frontActions() {
			add_shortcode('wc_product_categories_carousel', array($this, 'productCategoriesCarousel'));
		}
		function productCategoriesCarousel($atts, $content=null) {
			if(function_exists('wc_get_template')) {
				wc_get_template('product-categories-carousel.php');
			}
		}
	}
	$Dfd_WooCommerce_Ronneby_Init = new Dfd_WooCommerce_Ronneby_Init();
}