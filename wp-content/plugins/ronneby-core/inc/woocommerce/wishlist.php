<?php
if(!defined('ABSPATH')) {
	exit;
}

if(!class_exists('Dfd_Ronneby_Core_Wishlist_Init')) {
	class Dfd_Ronneby_Core_Wishlist_Init {
		function __construct() {
			$this->init();
		}
		function init() {
			add_shortcode('dfd_wishlist_button_shortcode', array($this, 'wishlistButtonShortcode'));
		}
		function wishlistAddToCart($label) {
			return '<i class="dfd-icon-shopping_bag_1"></i><span>'.$label.'</span>';
		}
	}
	$Dfd_Ronneby_Core_Wishlist_Init = new Dfd_Ronneby_Core_Wishlist_Init();
}