<?php
    /**
     * The template for the header sticky bar.
     * Override this template by specifying the path where it is stored (templates_path) in your Redux config.
     *
     * @author        Redux Framework
     * @package       ReduxFramework/Templates
     * @version:      3.5.7.8
     */
?>
<div id="redux-sticky">
    <div id="info_bar">

        <a href="javascript:void(0);" class="expand_options<?php echo esc_attr(( $this->parent->args['open_expanded'] ) ? ' expanded' : ''); ?>"<?php echo $this->parent->args['hide_expand'] ? ' style="display: none;"' : '' ?>>
            <?php esc_attr_e( 'Expand', 'dfd' ); ?>
        </a>

        <div class="redux-action_bar">
            <span class="spinner"></span>
            <?php if ( false === $this->parent->args['hide_save'] ) { ?>
                <?php submit_button( esc_attr__( 'Save Changes', 'dfd' ), 'primary', 'redux_save', false ); ?>
            <?php } ?>
            <?php if(isset($this->parent->options['dev_mode']) && $this->parent->options['dev_mode'] == 'on') : ?>
				<?php submit_button( __( 'Recompile All Styles', 'dfd' ), 'secondary', 'recompileStyleButton', false ,array("onclick"=>"return false;")); ?>
				<?php if ( false === $this->parent->args['hide_reset'] ) : ?>
					<?php // submit_button( __( 'Reset Section', 'dfd' ), 'secondary dfd-reset-button', $this->parent->args['opt_name'] . '[defaults-section]', false ); ?>
					<?php submit_button( __( 'Reset options', 'dfd' ), 'secondary dfd-reset-button', $this->parent->args['opt_name'] . '[defaults]', false ); ?>
				<?php endif; ?>
			<?php endif; ?>
        </div>
        <div class="redux-ajax-loading" alt="<?php esc_attr_e( 'Working...', 'dfd' ) ?>">&nbsp;</div>
        <div class="clear"></div>
    </div>

    <!-- Notification bar -->
    <div id="redux_notification_bar">
        <?php $this->notification_bar(); ?>
    </div>

	<div class="dfd-nav-tabs-section"></div>

</div>