<?php
/*
Plugin Name: Ronneby Theme Core
Plugin URI: http://rnbtheme.com/
Description: Ronneby theme core plugin. Contains DFD custom shortcodes for Visual Composer, custom post types and custom shortcodes
Version: 1.1.7
Author: DFD
Author URI: http://dfd.name/
*/

if(!defined('ABSPATH')) {
	exit;
}

define('DFD_RONNEBY_PLUGIN_URL', plugins_url().'/ronneby-core/');
define('DFD_RONNEBY_PLUGIN_PATH', plugin_dir_path(__FILE__));

class Dfd_Ronneby_Core_Plugin {
	/**
	 * Core singleton class
	 * @var self - pattern realization
	 */
	private static $_instance;
	/**
	 * A dummy constructor to prevent this class from being loaded more than once.
	 *
	 * @see Dfd_Ronneby_Core_Plugin::instance()
	 *
	 * @since 1.0
	 * @access private
	 * @dfd
	 */
	private function __construct() {
		/* We do nothing here! */
	}

	/**
	 * You cannot clone this class.
	 *
	 * @since 1.0
	 * @dfd
	 */
	public function __clone() {
		_doing_it_wrong(__FUNCTION__, esc_html__('Cheatin&#8217; huh?', 'dfd'), '1.0');
	}

	/**
	 * You cannot unserialize instances of this class.
	 *
	 * @since 1.0
	 * @dfd
	 */
	public function __wakeup() {
		_doing_it_wrong(__FUNCTION__, esc_html__('Cheatin&#8217; huh?', 'dfd'), '1.0');
	}

	/**
	 * The Dfd_Ronneby_Core_Plugin Instance
	 *
	 * @since 1.0
	 * @static
	 * @return object The one true Dfd_Ronneby_Core_Plugin.
	 * @dfd
	 */
	public static function instance() {
		if(is_null(self::$_instance)) {
			self::$_instance = new self();
			self::$_instance->init();
		}
		return self::$_instance;
	}
	
	/**
	 * Constructor
	 *
	 */
	public function init() {
		$theme = wp_get_theme()->get( 'Name' );
		$themeActive = get_site_option('dfd_ronneby_theme_activated', false);
		if($themeActive === 'active' && substr_count($theme, 'DFD Ronneby')) {
			add_action('plugins_loaded', array($this, 'pluginsLoaded'), 10);
			register_activation_hook(__FILE__, array($this, 'activationHook'));
			$this->loadComponents();
		} else {
			add_action('admin_notices', array($this, '_admin_notice__error'));
		}
	}
	
	/**
	 * Callback function WP plugin_loaded action hook. Loads locale
	 *
	 * @access public
	 */
	public function pluginsLoaded() {
		// Setup locale
		do_action('dfd_plugins_loaded');
	}
	
	/**
	 * Enables to add hooks in activation process.
	 *
	 */
	public function activationHook(  ) {
		do_action('dfd_activation_hook');
	}
	
	/**
	 * Load plugin components
	 *
	 */
	public function loadComponents(  ) {
		require_once(DFD_RONNEBY_PLUGIN_PATH .'inc/includes.php');
	}
	
	/*
	 * Admin notice text
	 */
	public function _admin_notice__error() {
		echo '<div class="notice notice-error is-dismissible">';
			echo '<p>'. esc_html__('Ronneby Core plugin is enabled but not effective. It requires Ronneby theme installed and activated in order to work.', 'dfd') .'</p>';
		echo '</div>';
	}
}

$Dfd_Ronneby_Core_Plugin = Dfd_Ronneby_Core_Plugin::instance();